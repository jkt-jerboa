/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_PLUGIN_H
#define _JERBOA_PLUGIN_H

#include "Types.h"
#include "TrackData.h"

class QWidget;

namespace Jerboa
{
	class PlayerInterface;
	/** Abstract class defining the plugin interface.
	 * All Jerboa plugins must inherit from this class.
	 */
	class Plugin
	{
		public:
			/** Gets the plugins name.
			 * This is potentially user-visible, so should be translated
			 */
			virtual const QString& pluginName() const = 0;

			/** Retrieves the plugins widget, if any.
			 * If a non-NULL pointer is returned, this widget will be added
			 * to the collection tab bar, using widget()->windowTitle() for the
			 * tab label.
			 */
			virtual QWidget* widget() { return NULL; }

			/** Gives the plugin a PlayerInterface object
			 * All communication with the player goes through this interface.
			 */
			virtual void setPlayerInterface(PlayerInterface*){};

			/** List of all components provided by the plugin.
			 * See ComponentInterface.
			 */
			virtual ComponentMap components(){ return ComponentMap(); }
	};
}

Q_DECLARE_INTERFACE(
	Jerboa::Plugin,
	"uk.co.fredemmott.jerboa.plugin/1.0"
);
#endif
