/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_PLAYER_INTERFACE_H
#define _JERBOA_PLAYER_INTERFACE_H

#include "Types.h"
#include "TrackData.h"

#include <QObject>
#include <QWidget>

namespace Jerboa
{
	class PlaylistInterface;

	/** Provides an interface for plugins to interact with the player.
	 * All interaction with the player is through this abstract class.
	 */
	class PlayerInterface : public QObject
	{
		Q_OBJECT;
		public:
			/** The current volume of the player.
			 * Between 0 and 1.
			 * \see setVolume
			 */
			virtual qreal volume() const = 0;
			/** The current volume of the player, in decibels.
			 * \see setVolumeDecibel
			 */
			virtual qreal volumeDecibel() const = 0;

			/** The position in the current track.
			 * Measured in milliseconds.
			 * \see setPosition
			 */
			virtual quint64 position() const = 0;

			/// An interface to the playlist.
			virtual PlaylistInterface* playlist() const = 0;
		public slots:
			virtual void play() = 0;
			virtual void pause() = 0;
			virtual void stop() = 0;
			virtual void skipNext() = 0;
			virtual void skipPrevious() = 0;

			/** Seeks to a position in the current track.
			 * \arg position is the time into the track to seek to, in milliseconds.
			 * \see position
			 */
			virtual void setPosition(quint64 position) = 0;

			/** Set the volume of the player.
			 * \arg v is the level to set to, between 0 and 1.
			 * \see volume
			 */
			virtual void setVolume(qreal v) = 0;

			/** Set the volume of the player, in decibels.
			 * \see setVolumeDecibels
			 */
			virtual void setVolumeDecibel(qreal) = 0;
		
			/// Set the looping mode of the player.
			virtual void setLoopMode(LoopMode) = 0;
			/// Set the shuffle mode of the player.
			virtual void setShuffleMode(ShuffleMode) = 0;

			/** The main window of the player.
			 * Do not cast this to a PlayerWindow object; the only supported operations
			 * on the result of this call are those inherited from QWidget.
			 */
			virtual QWidget* mainWindow() const = 0;
		signals:
			void loopModeChanged(LoopMode);
			void shuffleModeChanged(ShuffleMode);

			/** This signal is emitted when the actions the player is able to perform change.
			 * \arg available contains a bitwise OR of \ref Action flags.
			 */
			void availableActionsChanged(Actions available);
			
			/** This signal is emitted when the current track changes.
			 * Notably, it is emitted before playback actually starts,
			 * in between the two tracks, so could be useful to implement
			 * replaygain in a plugin.
			 *
			 * \arg newTrack is the \ref TrackData for the new track.
			 */
			void trackChanged(const TrackData& newTrack);

			/** This signal is emitted when playback of a new track starts.
			 * \arg track is the \ref TrackData for the new track.
			 * \arg length is the length of the track, in milliseconds.
			 */
			void playbackStarted(const TrackData& track, quint64 length);

			void playbackPaused();
			void playbackStopped();
			/**
			 * \see volume
			 * \see volumeDecibel
			 */
			void volumeChanged(qreal fraction, qreal db);
		protected:
			PlayerInterface(QObject* parent);
	};
}
#endif
