/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _UTIL_H
#define _UTIL_H

#include <QByteArray>
#include <QIcon>
#include <QObject>
#include <QString>
#include <QUrl>

class QFileInfo;

/** Get the name of an enum value from a static meta object.
 * @param so is a staticMetaObject.
 * @param e is an enum.
 * @param v is a value in this enum.
 * @returns a const char* containing the key of an enum.
 */
#define NAME_FROM_ENUM_HELPER(so,e,v) ( \
	so.enumerator( \
		so.indexOfEnumerator(#e) \
	).valueToKey(v) \
)
/** Get the value of an enum from it's key from a static meta object.
 * @param so is a staticMetaObject.
 * @param o is the object/class/namespace containing the enum declaration.
 * @param e is the enum name.
 * @param n is a QString containing the value key.
 * @returns An o::e.
 */
#define ENUM_FROM_NAME_HELPER(so,o,e,n) ( \
	static_cast<o::e>( \
		so.enumerator( \
			so.indexOfEnumerator(#e) \
		).keyToValue(n.toLatin1()) \
	) \
)
/** Get the key of a Qt namespace enum value.
 * @param e is the enum to get a value for, for example, ToolButtonStyle.
 * @param v is the value that a key is wanted for, for example, Qt::ToolButtonTextUnderIcons
 * @returns a const char* containing the key.
 */
#define QT_NAME_FROM_ENUM(e,v) NAME_FROM_ENUM_HELPER(QObject::staticQtMetaObject,e,v)
/** Get the value of a Qt namespace enum value from it's int value.
 * @param e is the enum to get a value for, for example, ToolButtonStyle.
 * @param n is the name of an enum value, for example "ToolButtonTextUnderIcons"
 * @returns A Qt::e value.
 */
#define QT_ENUM_FROM_NAME(e,n) ENUM_FROM_NAME_HELPER(QObject::staticQtMetaObject,Qt,e,n)
/** Get the key of a QObject's enum value.
 * @param o is a QObject
 * @param e is the enum to get a value for, for example, ToolButtonStyle.
 * @param v is the value that a key is wanted for, for example, Qt::ToolButtonTextUnderIcons
 * @returns a const char* containing the key.
 */
#define NAME_FROM_ENUM(o,e,v) NAME_FROM_ENUM_HELPER(o::staticMetaObject,e,v)
/** Get the value of a QObject enum value from it's int value.
 * @param o is a QObject
 * @param e is the enum to get a value for, for example, ToolButtonStyle.
 * @param n is the name of an enum value, for example "ToolButtonTextUnderIcons"
 * @returns A Qt::e value.
 */
#define ENUM_FROM_NAME(o,e,n) ENUM_FROM_NAME_HELPER(o::staticMetaObject,o,e,n)

namespace Util
{
	time_t timestamp();
	unsigned int getArtistID(QString artist, QString artistSort);
	QString artistName(unsigned int id);
	QString simpleAlbum(QString album);
	QString dataLocation();
	QString musicLocation();
	QIcon getIcon(QString name);
	QList<QUrl> walkDirectories(const QFileInfo& fileinfo);
};

#endif
