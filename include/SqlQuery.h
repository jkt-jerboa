/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_SQLQUERY_H
#define _JERBOA_SQLQUERY_H

#include <QSqlDatabase>
#include <QSqlQuery>

namespace Jerboa
{
	/** A subclass of QSqlQuery with added debugging features.
	 * When one of the \ref exec members is called, if an error
	 * occurs, information is printed to the debug stream.
	 *
	 * On non-Windows platforms, a backtrace is also printed.
	 *
	 * This class may later gain some automatic error-recovery
	 * features.
	 */
	class SqlQuery : public QSqlQuery
	{
		public:
			/// Contruct an SqlQuery on the default database.
			SqlQuery();
			/// Construct an SQL query on a specified database.
			SqlQuery(QSqlDatabase db);
			/// Execute the current query.
			bool exec();
			/// Execute the specified query.
			bool exec(const QString& query);
		private:
			void printError();
	};
}

#endif
