/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_PLAYLIST_INTERFACE_H
#define _JERBOA_PLAYLIST_INTERFACE_H

#include "TrackData.h"

#include <QObject>
#include <QUrl>

namespace Jerboa
{
	/// Provides an interface to the player's PlayList.
	class PlaylistInterface : public QObject
	{
		Q_OBJECT;
		public slots:
			/** Remove a track from the playlist.
			 * \arg position is the 0-indexed position of the track within the playlist.
			 */
			virtual void removeTrack(quint32 position) = 0;

			/** Add a track to the playlist.
			 * \arg url is the url of the media that should be added to the playlist.
			 * \returns the 0-indexed position in the playlist, or -1 if the insertion
			 * 	failed.
			 */
			virtual int appendTrack(QUrl url) = 0;

			/// The number of items in the playlist.
			virtual quint32 length() = 0;
		
			/** The position of the current track in the playlist.
			 * \returns the 0-indexed position, or -1 if there is no current track.
			 */
			virtual int currentTrack() = 0;

			/** Get information on the current track in the playlist.
			 * \arg position is the position of the track in the playlist.
			 * \see TrackData
			 */
			virtual const TrackData& trackData(quint32 position) = 0;
		signals:
			void modified();
		protected:
			PlaylistInterface(QObject* parent);
	};
}

#endif
