SET(
    PLUGINS
    InfoWidget
	Mpris
	ReplayGain
	Scrobbler
	TrayIcon
)

##############################################
# You probably don't want to edit below here #
##############################################
# Everything for the main UI is in core/CMakeLists.txt

# Turn the set of WITH_Foo_PLUGIN into an array of ACTIVE_PLUGINS
FOREACH(Plugin ${PLUGINS})
	OPTION("WITH_${Plugin}_PLUGIN" "Build and include the ${Plugin} Plugin" ON)
	IF(WITH_${Plugin}_PLUGIN)
		SET(ACTIVE_PLUGINS ${ACTIVE_PLUGINS} ${Plugin})
	ENDIF(WITH_${Plugin}_PLUGIN)
ENDFOREACH(Plugin ${PLUGINS})

# Add the subdirectories
ADD_SUBDIRECTORY(core)
ADD_SUBDIRECTORY(plugins)

# Add the images
QT4_ADD_RESOURCES( RESOURCE_SOURCES ../jerboa.qrc )

# Put Jerboa in the top level
SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_BINARY_DIR})

# Sources
ADD_EXECUTABLE(
    jerboa 
    WIN32
    ${RESOURCE_SOURCES}
)

TARGET_LINK_LIBRARIES(
    jerboa
    core
	${PHONON_LIBRARY}
    ${ACTIVE_PLUGINS}
    ${QT_LIBRARIES}
    ${TAGLIB_LIBRARY}
)

IF(APPLE)
	INSTALL(TARGETS jerboa DESTINATION MacOS)
	INSTALL(FILES ../images/jerboa.icns DESTINATION Resources)

	# Put qt in our appdir
	INSTALL_QT_FRAMEWORKS()
	INSTALL_QT_PLUGINS(sqldrivers/libqsqlite imageformats/libqgif imageformats/libqjpeg phonon_backend/libphonon_qt7 imageformats/libqsvg iconengines/libqsvgicon)

	# Relocate main executable's qt linkage
	RELOCATE_QT_LIBRARIES(MacOS/jerboa)

	# Install taglib to the appdir
	INSTALL_OSX_LIBRARY(${TAGLIB_LIBRARY})

	# Need the real thing we're linked against...
	GET_FILENAME_COMPONENT(TAGLIB_PATH ${TAGLIB_LIBRARY} PATH)
	GET_FILENAME_COMPONENT(TAGLIB_NAME ${TAGLIB_LIBRARY} NAME_WE)
	# Relocate Jerboa against local taglib
	RELOCATE_OSX_LIBRARY(${TAGLIB_PATH}/${TAGLIB_NAME}.1.dylib "@executable_path/libtag.dylib" MacOS/jerboa)

	# Make a dmg
	COMPRESS_PACKAGE(git-post-0.2)
ELSE(APPLE)
	INSTALL(TARGETS jerboa DESTINATION bin)
ENDIF(APPLE)
