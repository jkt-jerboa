/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "InfoWidget.h"
#include "InfoWidget_p.h"

#include "PlayerInterface.h"

#include <QtPlugin>

namespace Jerboa
{
	InfoWidget::InfoWidget() :
		QObject(),
		d(NULL)
	{
	} 

	void InfoWidget::trackChanged(const TrackData& t)
	{
		if ( t.isValid() )
			d->setTrack(t);
		else
			d->clear();
	}

	void InfoWidget::setPlayerInterface(PlayerInterface* pi)
	{
		connect(pi, SIGNAL(trackChanged(const TrackData&)), this, SLOT(trackChanged(const TrackData&)));
	}

	const QString& InfoWidget::pluginName() const
	{
		return d->pluginName;
	}

	QWidget* InfoWidget::widget()
	{
		if ( ! d ) d = new InfoWidgetPrivate();
		return d;
	}
};

Q_EXPORT_PLUGIN2(Jerboa_InfoWidget, Jerboa::InfoWidget);
