/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "InfoWidget_p.h"

#include "config.h"

#include <QAction>
#include <QApplication>
#include <QBuffer>
#include <QCryptographicHash>
#include <QDateTime>
#include <QDebug>
#include <QDesktopServices>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QFont>
#include <QMouseEvent>
#include <QRegExp>
#include <QStyle>

#include "Util.h"
namespace Jerboa
{
	InfoWidgetPrivate::InfoWidgetPrivate() :
		QWidget(),
		pluginName(tr("Track Information")),
		mCoverArtDir(Util::dataLocation() + "/coverImages"),
		mLyricsDir(Util::dataLocation() + "/lyrics"),
		mAwsKey("1JRV0F20598Z9ZAPF7R2"), // Please don't copy this to other programs; these keys are free from aws.amazon.com
		mCoverArtDownloader(new QNetworkAccessManager(this)),
		mLyricsDownloader(new QNetworkAccessManager(this))
	{
		QDir dir;
		dir.mkpath(mCoverArtDir);
		dir.mkpath(mLyricsDir);

		connect(mLyricsDownloader, SIGNAL(finished(QNetworkReply*)), this, SLOT(gotLyrics(QNetworkReply*)));

		ui.setupUi(this);
		// Setup UI
		int coverArtLength = 160 + (2 * ui.coverArtLabel->frameWidth());
		ui.coverArtLabel->setMaximumSize(coverArtLength, coverArtLength);
		ui.coverArtLabel->setMinimumSize(coverArtLength, coverArtLength);

		connect(
			ui.coverArtLabel, SIGNAL(customContextMenuRequested(const QPoint&)),
			this, SLOT(coverArtContextMenu(const QPoint&))
		);

		ui.creditsLabel->setText(tr("Cover art from <a href='http://%1'>%1</a>, lyrics from <a href='http://%2.org'>%2</a>.").arg("Amazon.com").arg("LyricWiki"));

		clear();

		mCoverArtMenu = new QMenu(this);
		QAction* a;
		a = new QAction(
			QApplication::style()->standardIcon(QStyle::SP_BrowserReload),
			tr("Refetch"),
			this
		);
		connect(a, SIGNAL(triggered()), this, SLOT(refetchCoverArt()));
		mCoverArtMenu->addAction(a);
		a = new QAction(
			QApplication::style()->standardIcon(QStyle::SP_DirOpenIcon),
			tr("Choose file.."),
			this
		);
		connect(a, SIGNAL(triggered()), this, SLOT(chooseCoverArt()));
		mCoverArtMenu->addAction(a);
	}

	void InfoWidgetPrivate::clear()
	{
		ui.coverArtLabel->setText(tr("No Album"));
		ui.trackLabel->setText(tr("No Track"));
		ui.artistLabel->setText(tr("No Artist"));
		ui.albumLabel->setText(tr("No Album"));
	
		ui.lyricsEdit->setDisabled(true);
		ui.lyricsEdit->setText(tr("No Lyrics"));

		mTD = TrackData();
	}

	QUrl InfoWidgetPrivate::getAWSUrl(QString album) const
	{
		QUrl awsUrl("http://webservices.amazon.com/onca/xml/");
		awsUrl.addQueryItem("Service", "AWSECommerceService");
		awsUrl.addQueryItem("Operation", "ItemSearch");
		awsUrl.addQueryItem("ResponseGroup", "Images");
		awsUrl.addQueryItem("SearchIndex", "Music");
		awsUrl.addQueryItem("AWSAccessKeyId", mAwsKey);
		awsUrl.addQueryItem("Title", album);
		return awsUrl;
	}

	void InfoWidgetPrivate::setTrack(const TrackData& t)
	{
		mTD = t;

		ui.trackLabel->setText(t.title());
		ui.artistLabel->setText(t.artist());
		ui.albumLabel->setText(t.album());
	
		mSimpleAlbum = Util::simpleAlbum(t.album());
	
		// If we've got cover art, use it; otherwise, try and download it
		mCoverArt.setFileName(QString("%1/%2.jpg").arg(mCoverArtDir).arg(
			QString(QCryptographicHash::hash(
				QString("%1#####%2").arg(t.albumArtist()).arg(mSimpleAlbum).toUtf8(),
				QCryptographicHash::Md5
			).toHex())
		));
		if ( ! mCoverArt.exists() )
			refetchCoverArt();
		else
			ui.coverArtLabel->setPixmap(QPixmap(mCoverArt.fileName(), "JPEG"));
	
		// See if we have lyrics
		ui.lyricsEdit->setDisabled(true);
		mLyrics.setFileName(QString("%1/%2.txt").arg(mLyricsDir).arg(
			QString(QCryptographicHash::hash(
				QString(
					"%1#####%2#####%3"
				).arg(
					t.artist()
				).arg(
					mSimpleAlbum
				).arg(
					t.title()
				).toUtf8(),
				QCryptographicHash::Md5
			).toHex())
		));

		if ( (! mLyrics.exists()) || (mLyrics.size() == 0 && Util::timestamp() - QFileInfo(mLyrics).lastModified().toUTC().toTime_t() > 24 * 60 * 60) )
		{
			ui.lyricsEdit->setText(tr("Trying to find lyrics."));
			QUrl lyricsUrl("http://lyricwiki.org/api.php");
			lyricsUrl.addQueryItem("fmt", "text");
			lyricsUrl.addQueryItem("artist", t.artist());
			lyricsUrl.addQueryItem("song", t.title());
			mLyricsDownloader->get(QNetworkRequest(lyricsUrl));
		}
		else if ( mLyrics.size() == 0 )
			ui.lyricsEdit->setText(tr("Could not find lyrics."));
		else
		{
			mLyrics.open(QIODevice::ReadOnly);
			ui.lyricsEdit->setHtml(QString::fromUtf8(mLyrics.readAll()));
			ui.lyricsEdit->setDisabled(false);
			mLyrics.close();
		}
	}

	void InfoWidgetPrivate::coverArtContextMenu(const QPoint& pos)
	{
		if ( mTD.isValid() )
			mCoverArtMenu->popup(ui.coverArtLabel->mapToGlobal(pos));
	}

	void InfoWidgetPrivate::gotCoverArtAwsXML(QNetworkReply* reply)
	{
		qDebug() << reply->url();
		if ( reply->error() != QNetworkReply::NoError )
		{
			ui.coverArtLabel->setText(tr("Couldn't Find Album Art"));
			return;
		}
		QByteArray data = reply->readAll();
	
		QString url = QUrl(QString(data).replace(QRegExp(".+<MediumImage><URL>([^<]+)<.+"), "\\1")).toString();
	
		if (! url.contains(QRegExp("^http://"))){
			if ( mCoverArtFirstTry )
			{
				QUrl awsUrl = getAWSUrl(ui.albumLabel->text());
				mCoverArtDownloader->get(QNetworkRequest(awsUrl));
				mCoverArtFirstTry = false;
				return;
			}
			// Okay, I'm stubborn
			QUrl mbUrl("http://musicbrainz.org/ws/1/release/");
			mbUrl.addQueryItem("type", "xml");
			mbUrl.addQueryItem("title", mSimpleAlbum);
			disconnect(mCoverArtDownloader, SIGNAL(finished(QNetworkReply*)), 0, 0);
			connect(mCoverArtDownloader, SIGNAL(finished(QNetworkReply*)), this, SLOT(gotCoverArtMbXML(QNetworkReply*)));
			mCoverArtDownloader->get(QNetworkRequest(mbUrl));
			return;
		}
	
		ui.coverArtLabel->setText(tr("Downloading Album Art"));
	
		disconnect(mCoverArtDownloader, SIGNAL(finished(QNetworkReply*)), 0, 0);
		connect(mCoverArtDownloader, SIGNAL(finished(QNetworkReply*)), this, SLOT(gotCoverArt(QNetworkReply*)));
		mCoverArtDownloader->get(QNetworkRequest(QUrl::fromEncoded(url.toUtf8())));
	}
	
	void InfoWidgetPrivate::gotCoverArtMbXML(QNetworkReply* reply)
	{
		qDebug() << reply->url();
		if ( reply->error() != QNetworkReply::NoError )
		{
			ui.coverArtLabel->setText(tr("Couldn't Find Album Art"));
			return;
		}
		QByteArray data = reply->readAll();
	
		QString asin = QUrl(QString(data).replace(QRegExp(".+<asin>([^<]+)<.+"), "\\1")).toString();
		if (! asin.contains(QRegExp("^[A-Z0-9]{10}$")) )
		{
			ui.coverArtLabel->setText(tr("Couldn't Find Album Art"));
			return;
		}
	
		QUrl awsUrl("http://webservices.amazon.com/onca/xml/");
		awsUrl.addQueryItem("Service", "AWSECommerceService");
		awsUrl.addQueryItem("Operation", "ItemLookup");
		awsUrl.addQueryItem("ResponseGroup", "Images");
		awsUrl.addQueryItem("AWSAccessKeyId", mAwsKey);
		awsUrl.addQueryItem("ItemId", asin);
	
		disconnect(mCoverArtDownloader, SIGNAL(finished(QNetworkReply*)), 0, 0);
		connect(mCoverArtDownloader, SIGNAL(finished(QNetworkReply*)), this, SLOT(gotCoverArtAwsXML(QNetworkReply*)));
		mCoverArtDownloader->get(QNetworkRequest(awsUrl));
	}
	
	void InfoWidgetPrivate::gotCoverArt(QNetworkReply* reply)
	{
		if ( reply->error() != QNetworkReply::NoError )
		{
			qDebug() << "Error:" << reply->error();
			qDebug() << "Content:" << reply->readAll();
			qDebug() << "URL:" << reply->url() << "- Encoded:" << reply->url().toEncoded();
			ui.coverArtLabel->setText(tr("Couldn't Download Album Art"));
			return;
		}
		mCoverArt.open(QIODevice::WriteOnly);
		mCoverArt.write(reply->readAll());
		mCoverArt.close();
		ui.coverArtLabel->setPixmap(QPixmap(mCoverArt.fileName(), "JPEG"));
	}
	
	void InfoWidgetPrivate::gotLyrics(QNetworkReply* reply)
	{
		bool error = reply->error() != QNetworkReply::NoError;
		QByteArray data = reply->readAll();
	
		if ( error || data == "Not found")
		{
			ui.lyricsEdit->setText(tr("Could not find lyrics."));
			if ( data == "Not found" )
			{
				// Update mtime
				mLyrics.open(QIODevice::WriteOnly);
				mLyrics.resize(1);
				mLyrics.resize(0);
				mLyrics.close();
			}
			return;
		}
		QStringList lines = QString::fromUtf8(data).split("\n");
		QMutableStringListIterator it(lines);
		bool inLyrics = true;
		while(it.hasNext())
		{
			QString line = it.next().trimmed();
			if (inLyrics && line == "</lyric>")
			{
				inLyrics = false;
				it.remove();
			}
			else if ( ! inLyrics )
			{
				if ( line == "<lyric>" )
					inLyrics = true;
				if ( line.left(2) == "==" && it.peekNext().trimmed() == "<lyric>" )
				{
					it.setValue("<br /><b>" + it.value().replace("=", "") + "</b>");
					continue;
				}
				it.remove();
			}
		}
		QString lyrics = lines.join("<br />\n");
		lyrics = lyrics.replace(QRegExp("'''([^']+)'''"), "<b>\\1</b>");
		lyrics = lyrics.replace(QRegExp("''([^']+)''"), "<i>\\1</i>");
	
		ui.lyricsEdit->setDisabled(false);
		ui.lyricsEdit->setHtml(lyrics);
	
		mLyrics.open(QIODevice::WriteOnly);
		mLyrics.write(lyrics.toUtf8());
		mLyrics.close();
	}

	void InfoWidgetPrivate::refetchCoverArt()
	{
		ui.coverArtLabel->setText(tr("Looking up Album"));
		QUrl awsUrl = getAWSUrl(mSimpleAlbum);
		awsUrl.addQueryItem("Artist", mTD.albumArtist());
		mCoverArtFirstTry = true;

		disconnect(mCoverArtDownloader, SIGNAL(finished(QNetworkReply*)), 0, 0);
		connect(mCoverArtDownloader, SIGNAL(finished(QNetworkReply*)), this, SLOT(gotCoverArtAwsXML(QNetworkReply*)));
		mCoverArtDownloader->get(QNetworkRequest(awsUrl));
	}

	void InfoWidgetPrivate::mousePressEvent(QMouseEvent* event)
	{
#ifdef EMBEDDED_LEFT_BUTTON_CONTEXT_MENU
		if(childAt(event->pos()) == ui.coverArtLabel)
			mCoverArtMenu->popup(event->globalPos());
#else
		Q_UNUSED(event);
#endif
	}

	void InfoWidgetPrivate::chooseCoverArt()
	{
		QString openPath = QFileDialog::getOpenFileName(
				this,
				tr("Select Cover Art"),
				QDesktopServices::storageLocation(QDesktopServices::HomeLocation),
				tr("Images (*.gif *.jpg *.jpeg *.png)")
		);
		QPixmap loader;
		if ( loader.load(openPath) )
		{
			loader = loader.scaled(160, 160, Qt::KeepAspectRatio, Qt::SmoothTransformation);
			loader.save(mCoverArt.fileName(), "JPEG");
			ui.coverArtLabel->setPixmap(loader);
		}
	}
};

