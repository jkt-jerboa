/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_INFO_WIDGET_P_H
#define _JERBOA_INFO_WIDGET_P_H

#include "TrackData.h"

#include <QByteArray>
#include <QFile>
#include <QMenu>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>
#include <QUrl>

#include "ui_InfoWidget.h"

namespace Jerboa
{
	class InfoWidgetPrivate : public QWidget
	{
		Q_OBJECT;
		public:
			InfoWidgetPrivate();

			void clear();
			void setTrack(const TrackData& t);
			const QString pluginName;
		private slots:
			void gotCoverArtAwsXML(QNetworkReply*); // Amazon Web Service
			void gotCoverArtMbXML(QNetworkReply*); // MusicBrainz
			void gotCoverArt(QNetworkReply*);
			void gotLyrics(QNetworkReply*);
			void coverArtContextMenu(const QPoint& pos);
			void refetchCoverArt();
			void chooseCoverArt();
		protected:
			void mousePressEvent(QMouseEvent* event);
		private:
			// Functions
			QUrl getAWSUrl(QString album) const;

			// Constants
			const QString mCoverArtDir;
			const QString mLyricsDir;
			const QString mAwsKey;

			// Current Track
			TrackData mTD;

			// Other member variables
			Ui_InfoWidget ui;
			QString mSimpleAlbum;
			QFile mCoverArt;
			QNetworkAccessManager* mCoverArtDownloader;
			bool mCoverArtFirstTry;
			QFile mLyrics;
			QNetworkAccessManager* mLyricsDownloader;

			QMenu* mCoverArtMenu;
	};
}
#endif
