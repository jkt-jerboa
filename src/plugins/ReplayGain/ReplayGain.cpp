/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ReplayGain.h"

#include "PlayerInterface.h"

#include <QtPlugin>
#include <QSettings>

namespace Jerboa
{
	ReplayGain::ReplayGain()
		:
			QObject(0),
			mPluginName(tr("ReplayGain")),
			mPI(NULL),
			mUsingRG(true)
	{}

	const QString& ReplayGain::pluginName() const { return mPluginName; }
	
	void ReplayGain::setPlayerInterface(PlayerInterface* pi)
	{
		QSettings settings;
		mGain = settings.value("replaygain/gain", "8").toString().toFloat();
		mDefaultGain = settings.value("replaygain/defaultReplayGain", "-3").toString().toFloat();
		mRGMode = (ReplayGainMode) settings.value("replaygain/mode", AlbumMode).toInt();
		mUseReplayGain = settings.value("replaygain/enabled", true).toBool();

		if ( mUseReplayGain )
			connect(pi, SIGNAL(trackChanged(const TrackData&)), this, SLOT(trackChanged(const TrackData&)));
		pi->setVolumeDecibel(mGain);
		mPI = pi;
	}

	void ReplayGain::trackChanged(const TrackData& td)
	{
		Q_ASSERT(mPI);
		if ( ! td.isValid() ) return;

		qreal volume;
	
		if ( mRGMode == AlbumMode )
			volume = mGain - td.albumRG();
		else
			volume = mGain - td.trackRG();
	
		if ( volume > 9000 )
		{
			if ( mUsingRG == false )
				return;
			mUsingRG = false;
			volume = mGain - mDefaultGain;
			mPI->setVolumeDecibel(volume);
			return;
		}
		mUsingRG = true;
		mPI->setVolumeDecibel(volume);
	}
};

Q_EXPORT_PLUGIN2(Jerboa_ReplayGain, Jerboa::ReplayGain);
