To make a new plugin:
- Inherit from both Jerboa::Plugin and QObject
- Your header must include Q_INTERFACES(Jerboa::Plugin)
- Your .cpp must include Q_EXPORT_PLUGIN2(PluginName, Class)
- Edit src/core/StaticPlugins.h to import your plugin
- Edit the STATIC_PLUGINS variable in src/CMakeLists.txt to make Jerboa link
	against your plugin
- Make sure your pluginName function has both const keywords
- Make sure that any slots you override from Jerboa::Plugin are slots in your
	plugin - otherwise, Jerboa won't call them.
- Only use the classes defined in include/
