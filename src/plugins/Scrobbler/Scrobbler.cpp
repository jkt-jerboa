/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Scrobbler.h"

#include "PlayerInterface.h"
#include "SqlQuery.h"
#include "Util.h"

#include <QtPlugin>
#include <QCryptographicHash>
#include <QDebug>
#include <QSettings>
#include <QSqlRecord>
#include <QStringList>

namespace Jerboa
{
	Scrobbler::Scrobbler() : QObject(), mPluginName(tr("Last.fm AudioScrobbler"))
	{
		mFailTime = false;
		mOnline = false;
		mSubmitReply = NULL;
		mHaveAuthData = false;
	};

	void Scrobbler::setPlayerInterface(PlayerInterface* pi)
	{
		connect(
			pi, SIGNAL(playbackStarted(const TrackData&, quint64)),
			this, SLOT(playbackStarted(const TrackData&, quint64))
		);
		connect(
			pi, SIGNAL(playbackPaused()),
			this, SLOT(playbackPaused())
		);
		connect(
			pi, SIGNAL(playbackStopped()),
			this, SLOT(playbackStopped())
		);
		QSettings settings;
		QString user = settings.value("lastfm/user", "").toString();
		QString password = settings.value("lastfm/password", "").toString();

		if ( user.isEmpty() )
		{
			mHaveAuthData = false;
			return;
		}

		mHaveAuthData = true;
		mUser = user;
		mPassword = password;
		login();
	}

	void Scrobbler::login()
	{
		if ( ! mHaveAuthData ) return;
		if ( (Util::timestamp() - mFailTime) < 30 ) return;
		mFailTime = 0;
		mOnline = false;

		qDebug() << "Starting Login";

		QString timestamp = QString::number(Util::timestamp());

		QUrl initURL("http://post.audioscrobbler.com/");
		initURL.addQueryItem("hs", "true"); // handshake
		initURL.addQueryItem("p", "1.2"); // protocol version
		
		// If you're looking at this code for work on another player,
		// please don't copy the client and client version - ask for
		// your own ID from Russ at last.fm - in the mean time, you can
		// use "tst" version "1.0" for testing.
	
		initURL.addQueryItem("c", "yan"); // client
		initURL.addQueryItem("v", "0.1"); // client version
		initURL.addQueryItem("u", mUser); // user
		initURL.addQueryItem("t", timestamp); // timestamp
		initURL.addQueryItem("a", QCryptographicHash::hash(
					QString(mPassword + timestamp).toUtf8(),
					QCryptographicHash::Md5).toHex()
		); // Auth token
	
		QNetworkReply* reply = mManager.get(QNetworkRequest(initURL));
		connect(reply, SIGNAL(finished()), this, SLOT(loginFinished()));
		connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
	}

	void Scrobbler::error(QNetworkReply::NetworkError error)
	{
		QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
		Q_ASSERT(reply);

		qDebug() << "An error occured:" << error;
		qDebug() << "URL:" << reply->url();
		qDebug() << "Operation:" << reply->operation();
		mFailTime = Util::timestamp();
		mOnline = false;
	}

	void Scrobbler::loginFinished()
	{
		QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
		Q_ASSERT(reply);

		QString data = reply->readAll();
		QStringList lines = data.split("\n");
		if ( lines[0] != "OK" )
		{
			qDebug() << "LastFM auth failed:" << data << reply->error();
			if ( lines[0] != "BANNED" && lines[0] != "BADAUTH" && lines[0] != "BADTIME" )
				login();
			return;
		}
		qDebug() << "LastFM auth successful";
	
		mSession = lines[1];
		mNowPlayingUrl.setUrl(lines[2]);
		mSubmitUrl.setUrl(lines[3]);

		mOnline = true;
	
		submit();
		reply->close();
	}

	void Scrobbler::submit()
	{
		if ( ! mOnline ){
			login();
			return;
		}
		if ( mSubmitReply ) return;

		SqlQuery query;
		query.exec("SELECT `TimeStamp`, `Length`, `Artist`, `Album`, `Name`, `TrackNumber`, `MBID` FROM `LastFMCache` ORDER BY `TimeStamp` ASC LIMIT 50");
		if ( ! query.first() ) return;
			
		unsigned int i = 0;
		QUrl submitURL(mSubmitUrl);
		submitURL.addQueryItem("s", mSession);
		do
		{
			QSqlRecord r = query.record();
			time_t timestamp = r.value(0).value<time_t>();
			quint64 length = r.value(1).value<quint64>();
			QString artist = r.value(2).toString();
			QString album = r.value(3).toString();
			QString title = r.value(4).toString();
			unsigned int trackNumber = r.value(5).toUInt();
			QString mbid = r.value(6).toString();
	
			submitURL.addQueryItem(QString("a[%1]").arg(i), artist);
			submitURL.addQueryItem(QString("t[%1]").arg(i), title);
			submitURL.addQueryItem(QString("n[%1]").arg(i), trackNumber == 0 ? "" : QString::number(trackNumber));
			submitURL.addQueryItem(QString("m[%1]").arg(i), mbid);
			submitURL.addQueryItem(QString("b[%1]").arg(i), album);
			submitURL.addQueryItem(QString("i[%1]").arg(i), QString::number(timestamp));
			submitURL.addQueryItem(QString("o[%1]").arg(i), "P");
			submitURL.addQueryItem(QString("r[%1]").arg(i), "");
			submitURL.addQueryItem(QString("l[%1]").arg(i), QString::number(length));
	
			mSubmitLast = timestamp;
			i++;
		} while(query.next());
	
		QByteArray data = QString(submitURL.encodedQuery().replace("%5B", "[").replace("%5D", "]")).toUtf8();

		QNetworkRequest request(mSubmitUrl);
		request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

		mSubmitReply = mManager.post(request, data);
		connect(mSubmitReply, SIGNAL(finished()), this, SLOT(submitFinished()));
		connect(mSubmitReply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
	}

	void Scrobbler::submitFinished()
	{
		QString data = mSubmitReply->readAll();
		mSubmitReply->close();
		QStringList lines = data.split("\n");
		mSubmitReply = NULL;

		if ( lines[0] != "OK" )
		{
			qDebug() << "Failed submission:" << data;
			if ( lines[0] == "BADSESSION" )
			{
				qDebug() << "Retrying login...";
				mOnline = false;
				login();
			}
			return;
		}
		SqlQuery query;
		query.prepare(QString("DELETE FROM `LastFMCache` WHERE `TimeStamp` <= %1").arg(mSubmitLast));
		query.exec();
		qDebug() << "Flushed last.fm cache block";
		submit();
	}

	void Scrobbler::playbackStarted(const TrackData& track, quint64 length)
	{
		if ( mOnline ) // Last.fm "Now Playing"
		{
			QUrl submitURL(mNowPlayingUrl);
			submitURL.addQueryItem("s", mSession);
			submitURL.addQueryItem("a", track.artist());
			submitURL.addQueryItem("t", track.title());
			submitURL.addQueryItem("b", Util::simpleAlbum(track.album()));
			submitURL.addQueryItem("n", track.trackNumber() != 0 ? QString::number(track.trackNumber()) : "");
			submitURL.addQueryItem("m", track.musicBrainzID() != "NONE" ? track.musicBrainzID() : "");
			submitURL.addQueryItem("l", QString::number(length));
			QByteArray data = QString(submitURL.encodedQuery()).toUtf8();
	
			QNetworkRequest request(mNowPlayingUrl);
			request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
	
			QNetworkReply* reply = mManager.post(request, data);
			connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
		}

		// Track playback time for scrobbling

		if ( mCurrentTrack.td == track ) // unpause
		{
			mStartTime += Util::timestamp() - mPauseTime;
			mPauseTime = 0;
			return;
		}

		mCurrentTrack.td = track;
		mCurrentTrack.length = length;

		mStartTime = Util::timestamp();
		mPauseTime = 0;
	};

	void Scrobbler::playbackPaused(){ mPauseTime = Util::timestamp(); }

	void Scrobbler::queueTrack()
	{
		if ( ! mCurrentTrack.td.isValid() ) return;
		quint64 total = mCurrentTrack.length;
		if ( mStartTime != 0 &&	total > 30 && Util::timestamp() - mStartTime > qMin<time_t>(240, total / 2000) )
		{
			SqlQuery query;	
			query.prepare("INSERT INTO `LastFMCache` (`TimeStamp`, `Length`, `Artist`, `Album`, `Name`, `TrackNumber`, `MBID`) \
					VALUES(:timestamp, :length, :artist, :album, :title, :trackNumber, :mbid)");
			query.bindValue(":timestamp", QString::number(Util::timestamp()));
			query.bindValue(":length", QString::number(total));
	
			TrackData td = mCurrentTrack.td;
			query.bindValue(":artist", td.artist());
			query.bindValue(":album", Util::simpleAlbum(td.album()));
			query.bindValue(":title", td.title());
			query.bindValue(":trackNumber", td.trackNumber());
			query.bindValue(":mbid", td.musicBrainzID().isEmpty() ? "" : td.musicBrainzID());
			query.exec();
			qDebug() << "Added to LastFM cache.";
		}
		else
			qDebug() << "Track didn't meet last.fm playing requirements" << mStartTime << total << Util::timestamp();
	}

	void Scrobbler::playbackStopped()
	{
		queueTrack();
		submit();
		mCurrentTrack.td = TrackData();
	}

	Scrobbler::~Scrobbler()
	{
		queueTrack();
	}
	
	const QString& Scrobbler::pluginName() const
	{
		return mPluginName;
	}
};

Q_EXPORT_PLUGIN2(Jerboa_Scrobbler, Jerboa::Scrobbler);
