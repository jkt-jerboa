
/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _SCROBBLER_H
#define _SCROBBLER_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QString>
#include <QUrl>

#include "TrackData.h"
#include "Plugin.h"

namespace Jerboa
{
	class Scrobbler : public QObject, public Plugin
	{
		Q_OBJECT;
		Q_INTERFACES(Jerboa::Plugin);
		private:
			bool mHaveAuthData;
			bool mOnline;

			QString mUser;
			QString mPassword;
			QString mSession;
			const QString mPluginName;

			QUrl mNowPlayingUrl;
			QUrl mSubmitUrl;

			time_t mSubmitLast;
			QNetworkReply* mSubmitReply;

			QNetworkAccessManager mManager;

			time_t mStartTime;
			time_t mPauseTime;
			time_t mFailTime;
		
			struct lfmData
			{
				TrackData td;
	 			quint64 length;
			} mCurrentTrack;

			void login();
			void submit();
			void queueTrack();
		private slots:
			void loginFinished();
			void submitFinished();
			void error(QNetworkReply::NetworkError code);
		public:
			Scrobbler();
			~Scrobbler();
			const QString& pluginName() const;
			void setPlayerInterface(PlayerInterface*);
		private slots:
			void playbackStarted(const TrackData& track, quint64 length);
			void playbackPaused();
			void playbackStopped(); // or end is reached
	};
};

#endif
