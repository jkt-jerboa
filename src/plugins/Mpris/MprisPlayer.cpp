/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "MprisPlayer.h"

#include "Types.h"
#include "MprisPlayerAdaptor.h"

namespace Jerboa
{
	MprisPlayer::MprisPlayer(PlayerInterface* pi, QObject* parent)
		:
			QObject(parent),
			mprisActionMask(SkipNextAction | SkipPreviousAction | PauseAction | PlayAction),
			mprisActionExtra(GetMetadataAction | GetTracklistAction),
			mPi(pi)
	{
		mState.playState = 2;
		mState.randomState = 0;
		mState.repeatState = 0;
		mState.isFinite = 0;
		mLoopMode = LoopNone;
		new MprisPlayerAdaptor(this);

		connect(pi, SIGNAL(loopModeChanged(LoopMode)), this, SLOT(loopModeChanged(LoopMode)));
		connect(pi, SIGNAL(shuffleModeChanged(ShuffleMode)), this, SLOT(shuffleModeChanged(ShuffleMode)));
		connect(pi, SIGNAL(availableActionsChanged(Actions)), this, SLOT(updateActions(Actions)));
		connect(pi, SIGNAL(playbackStarted(const TrackData&, quint64)), this, SLOT(playbackStarted(const TrackData&, quint64)));
		connect(pi, SIGNAL(playbackPaused()), this, SLOT(playbackPaused()));
		connect(pi, SIGNAL(playbackStopped()), this, SLOT(playbackStopped()));

		QDBusConnection::sessionBus().registerObject("/Player", this);
	}

	void MprisPlayer::updateActions(Actions a)
	{
		mActions = a;
		Actions value = ((a & mprisActionMask) | mprisActionExtra);
		if ( mState.playState != 2 )  value |= SeekAction;
		emit CapsChange(value);
	}

	void MprisPlayer::Play() { if(mActions & PlayAction) mPi->play(); }
	void MprisPlayer::Pause() { if(mActions & PauseAction) mPi->pause(); }
	void MprisPlayer::Stop() { if(mActions & StopAction) mPi->stop(); }
	void MprisPlayer::Next() { if(mActions & SkipNextAction) mPi->skipNext(); }
	void MprisPlayer::Prev() { if(mActions & SkipPreviousAction) mPi->skipPrevious(); }
	
	void MprisPlayer::Repeat(bool r)
	{
		mPi->setLoopMode(r ? LoopTrack : LoopNone);
		mState.repeatState = r ? 1 : 0;
		emit StatusChange(mState);
	}

	QVariantMap MprisPlayer::GetMetadata() const
	{
		return mMetadata;
	}

	int MprisPlayer::GetCaps() const
	{
		Actions a = ((mActions & mprisActionMask) | mprisActionExtra);
		if ( mState.playState != 2 ) a |= SeekAction;
		return a;
	}

	void MprisPlayer::playbackStarted(const TrackData& newTrack, quint64 length)
	{
		mMetadata = newTrack.toMpris();
		mMetadata["mtime"] = quint32(length);
		mMetadata["time"] = quint32(length / 1000);
		emit TrackChange(mMetadata);

		mState.playState = 0;
		emit StatusChange(mState);
	}

	int MprisPlayer::PositionGet() const
	{
		return int(mPi->position());
	}

	void MprisPlayer::PositionSet(int v)
	{
		mPi->setPosition(quint64(v));
	}

	void MprisPlayer::playbackPaused()
	{
		mState.playState = 1;
		emit StatusChange(mState);
	}

	void MprisPlayer::playbackStopped()
	{
		mState.playState = 2;
		emit StatusChange(mState);
	}

	const MprisState& MprisPlayer::GetStatus() const
	{
		return mState;
	}

	void MprisPlayer::VolumeSet(int v)
	{
		mPi->setVolume(qreal(v / qreal(100.0)));
	}

	int MprisPlayer::VolumeGet() const
	{
		return int(mPi->volume() * 100);
	}

	void MprisPlayer::loopModeChanged(LoopMode lm)
	{
		mLoopMode = lm;
		updateLoopState();
	}

	void MprisPlayer::shuffleModeChanged(ShuffleMode sm)
	{
		mShuffleMode = sm;
		updateLoopState();
	}

	void MprisPlayer::updateLoopState()
	{
		mState.randomState = mShuffleMode == ShuffleNone ? 0 : 1;
		mState.repeatState = mLoopMode == LoopTrack ? 1 : 0;
		mState.isFinite = mLoopMode == LoopPlaylist || mShuffleMode != ShuffleNone ? 1 : 0;
		emit StatusChange(mState);
	}
}

QDBusArgument &operator<<(QDBusArgument& a, const MprisState& s)
{
	a.beginStructure();
	a << s.playState << s.randomState << s.repeatState << s.isFinite;
	a.endStructure();
	return a;
}

const QDBusArgument &operator>>(const QDBusArgument& a, MprisState& s)
{
	a.beginStructure();
	a >> s.playState >> s.randomState >> s.repeatState >> s.isFinite;
	a.endStructure();
	return a;
}
