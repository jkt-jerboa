/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_MPRIS_H
#define _JERBOA_MPRIS_H

#include "Plugin.h"

namespace Jerboa
{
	class Mpris: public QObject, public Plugin
	{
		Q_OBJECT;
		Q_INTERFACES(Jerboa::Plugin);
		public:
			Mpris();
			const QString& pluginName() const;
			void setPlayerInterface(PlayerInterface*);
		private:
			const QString mPluginName;
	};
}

#endif
