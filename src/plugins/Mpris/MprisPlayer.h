/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_MPRIS_PLAYER
#define _JERBOA_MPRIS_PLAYER

#include "PlayerInterface.h"

#include <QDBusAbstractAdaptor>
#include <QDBusArgument>
#include <QVariant>

// Defined in MPRIS spec
struct MprisState
{
	int playState; // 0->playing, 1->paused, 2->stopped
	int randomState; // 0->linear, 1->shuffle
	int repeatState; // 0->linear, 1->loop track
	int isFinite; // 0->end at end of playlist, 1->loop playlist (or some kind of shuffle)
};

QDBusArgument &operator<<(QDBusArgument&, const MprisState&);
const QDBusArgument &operator>>(const QDBusArgument&, MprisState&);
Q_DECLARE_METATYPE(MprisState);

namespace Jerboa
{
	class MprisPlayer : public QObject
	{
		Q_OBJECT;
		Q_CLASSINFO("D-Bus Interface", "org.freedesktop.MediaPlayer");
		public:
			MprisPlayer(PlayerInterface* pi, QObject* parent = NULL);
		public slots:
			void Play();
			void Pause();
			void Stop();
			void Next();
			void Prev();
			void Repeat(bool);
			int GetCaps() const;
			const MprisState& GetStatus() const;
			QVariantMap GetMetadata() const;
			int VolumeGet() const;
			void VolumeSet(int);
			int PositionGet() const;
			void PositionSet(int);
		signals:
			void StatusChange(MprisState);
			void CapsChange(int);
			void TrackChange(QVariantMap);
		private slots:
			void loopModeChanged(LoopMode);
			void shuffleModeChanged(ShuffleMode);
			void updateActions(Actions available);
			void playbackStarted(const TrackData& track, quint64 length);
			void playbackPaused();
			void playbackStopped();
		private:
			const Actions mprisActionMask;
			const Actions mprisActionExtra;
			LoopMode mLoopMode;
			ShuffleMode mShuffleMode;
			QVariantMap mMetadata;
			Actions mActions;
			MprisState mState;
			PlayerInterface* mPi;

			void updateLoopState();
	};
}
#endif
