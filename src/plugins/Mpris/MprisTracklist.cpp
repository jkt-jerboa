/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "MprisTracklist.h"

#include "PlayerInterface.h"
#include "PlaylistInterface.h"

#include "MprisTracklistAdaptor.h"

#include <QDBusConnection>

namespace Jerboa
{
	MprisTracklist::MprisTracklist(PlayerInterface* pI, PlaylistInterface* pLI, QObject* parent)
		:
			QObject(parent),
			mPI(pI),
			mPLI(pLI)
	{
		connect(
			mPLI,
			SIGNAL(modified()),
			this,
			SLOT(modified())
		);

		new MprisTracklistAdaptor(this);
		QDBusConnection::sessionBus().registerObject("/TrackList", this);
	}

	QVariantMap MprisTracklist::GetMetadata(int position)
	{
		return mPLI->trackData(position).toMpris();
	}

	int MprisTracklist::GetCurrentTrack()
	{
		return mPLI->currentTrack();
	}

	int MprisTracklist::GetLength()
	{
		return mPLI->length();
	}

	int MprisTracklist::AddTrack(QString url, bool skipTo)
	{
		int retVal = mPLI->appendTrack(QUrl(url));
		if ( retVal != -1 )
		{
			//FIXME: handle skipTo == true
			return 0;
		}
		return -1;
	}

	void MprisTracklist::DelTrack(int position)
	{
		if (position < 0) return;
		mPLI->removeTrack(position);
	}

	void MprisTracklist::SetLoop(bool looping)
	{
		mPI->setLoopMode( looping ? LoopPlaylist : LoopNone );
	}

	void MprisTracklist::SetRandom(bool shuffleTracks)
	{
		mPI->setShuffleMode( shuffleTracks ? ShuffleTracks : ShuffleNone );
	}

	void MprisTracklist::modified()
	{
		emit TrackListChange(GetLength());
	}
};
