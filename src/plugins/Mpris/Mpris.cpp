/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Mpris.h"

#include <QtDBus>
#include <QtPlugin>
#include <QCoreApplication>
#include <QDBusConnection>
#include <QDebug>

#include "MprisIdentification.h"
#include "MprisPlayer.h"
#include "MprisTracklist.h"

namespace Jerboa
{
	Mpris::Mpris()
		:
			QObject(),
			mPluginName(tr("DBUS/MPRIS Interface"))
	{
		qDBusRegisterMetaType<MprisState>();
		qDBusRegisterMetaType<MprisSpecVersion>();
	}

	void Mpris::setPlayerInterface(PlayerInterface* pi)
	{
		QDBusConnection::sessionBus().registerService("org.mpris." + QCoreApplication::applicationName().toLower());
		new MprisIdentification(this);
		new MprisPlayer(pi, this);
		new MprisTracklist(pi, pi->playlist(), this);
	}

	const QString& Mpris::pluginName() const
	{
		return mPluginName;
	}
};

Q_EXPORT_PLUGIN2(Jerboa_Mpris, Jerboa::Mpris);
