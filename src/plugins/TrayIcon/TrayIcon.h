/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_TRAY_ICON_H
#define _JERBOA_TRAY_ICON_H

#include "Plugin.h"

#include <QSystemTrayIcon>

class QMenu;
class QAction;

namespace Jerboa
{
	class TrayIcon : public QObject, public Plugin
	{
		Q_OBJECT;
		Q_INTERFACES(Jerboa::Plugin);
		public:
			TrayIcon();
			~TrayIcon();
			const QString& pluginName() const;
			void setPlayerInterface(PlayerInterface*);
		protected:
			bool eventFilter(QObject* obj, QEvent* event);
		private slots:
			void playbackStarted(const TrackData& track, quint64 length);
			void playbackPaused();
			void playbackStopped();

			void activated(QSystemTrayIcon::ActivationReason);
			void updateActions(Actions availableActions);
		private:
			QMenu* mMenu;
			QAction* mPlayAction;
			QAction* mPauseAction;
			QAction* mStopAction;
			QAction* mNextAction;
			QAction* mPreviousAction;

			QString mTrackLine;
			const QString mPluginName;
			PlayerInterface* mPlayer;
			QSystemTrayIcon* mTrayIcon;
	};
}

#endif
