/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "TrayIcon.h"

#include "config.h"
#include "PlayerInterface.h"
#include "Util.h"

#include <QtPlugin>
#include <QAction>
#include <QApplication>
#include <QWheelEvent>
#include <QIcon>
#include <QMenu>
#include <QWidget>

namespace Jerboa
{
	TrayIcon::TrayIcon()
		:
			QObject(),
			mMenu(NULL),
			mPluginName(tr("Tray Icon")),
			mPlayer(NULL),
			mTrayIcon(new QSystemTrayIcon(this))
	{
		connect(
			mTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
			this, SLOT(activated(QSystemTrayIcon::ActivationReason))
		);
		mTrayIcon->installEventFilter(this);
	}

	TrayIcon::~TrayIcon()
	{
		delete mMenu;
	}

	const QString& TrayIcon::pluginName() const
	{
		return mPluginName;
	}

	void TrayIcon::setPlayerInterface(PlayerInterface* pi)
	{
		mPlayer = pi;
		setParent(pi->mainWindow());

		connect(
			pi, SIGNAL(availableActionsChanged(Actions)),
			this, SLOT(updateActions(Actions))
		);
		connect(
			pi, SIGNAL(playbackStarted(const TrackData&, quint64)),
			this, SLOT(playbackStarted(const TrackData&, quint64))
		);
		connect(
			pi, SIGNAL(playbackPaused()),
			this, SLOT(playbackPaused())
		);
		connect(
			pi, SIGNAL(playbackStopped()),
			this, SLOT(playbackStopped())
		);

		playbackStopped();
		mTrayIcon->show();

		mPlayAction = new QAction(Util::getIcon("media-playback-start"), tr("Play"), this);
		mPauseAction = new QAction(Util::getIcon("media-playback-pause"), tr("Pause"), this);
		mStopAction = new QAction(Util::getIcon("media-playback-stop"), tr("Stop"), this);
		mNextAction = new QAction(Util::getIcon("media-skip-forward"), tr("Next"), this);
		mPreviousAction = new QAction(Util::getIcon("media-skip-backward"), tr("Previous"), this);

		QAction* quitAction = new QAction(tr("Quit"), this);

		mMenu = new QMenu();
		mMenu->addAction(mPlayAction);
		mMenu->addAction(mPauseAction);
		mMenu->addAction(mStopAction);
		mMenu->addAction(mNextAction);
		mMenu->addAction(mPreviousAction);
		mMenu->addSeparator();
		mMenu->addAction(quitAction);
		mTrayIcon->setContextMenu(mMenu);

		connect(mPlayAction, SIGNAL(triggered()), pi, SLOT(play()));
		connect(mPauseAction, SIGNAL(triggered()), pi, SLOT(pause()));
		connect(mStopAction, SIGNAL(triggered()), pi, SLOT(stop()));
		connect(mNextAction, SIGNAL(triggered()), pi, SLOT(skipNext()));
		connect(mPreviousAction, SIGNAL(triggered()), pi, SLOT(skipPrevious()));
		connect(quitAction, SIGNAL(triggered()), QApplication::instance(), SLOT(quit()));
	}

	void TrayIcon::playbackStarted(const TrackData& track, quint64 length)
	{
		mTrayIcon->showMessage(
				track.title(),
				track.album() + "\n" + track.artist()
		);
		mTrayIcon->setIcon(Util::getIcon("media-playback-start"));
		mTrackLine = QString("<b>%1</b><br />%2<br />%3").arg(track.title()).arg(track.album()).arg(track.artist());
		mTrayIcon->setToolTip(mTrackLine);
	}

	void TrayIcon::playbackPaused()
	{
		mTrayIcon->setIcon(Util::getIcon("media-playback-pause"));
		mTrayIcon->setToolTip(tr("Paused:<br />%1").arg(mTrackLine));
	}

	void TrayIcon::playbackStopped()
	{
		mTrayIcon->setIcon(Util::getIcon("media-playback-stop"));
		mTrayIcon->setToolTip(tr("Stopped"));
	}

	void TrayIcon::updateActions(Actions actions)
	{
		mPlayAction->setEnabled(actions & PlayAction);
		mPauseAction->setEnabled(actions & PauseAction);
		mStopAction->setEnabled(actions & StopAction);
		mNextAction->setEnabled(actions & SkipNextAction);
		mPreviousAction->setEnabled(actions & SkipPreviousAction);
	}
	
	void TrayIcon::activated(QSystemTrayIcon::ActivationReason a)
	{
		if ( a == QSystemTrayIcon::Trigger )
		{
#ifdef EMBEDDED_LEFT_BUTTON_CONTEXT_MENU
			mTrayIcon->contextMenu()->popup(mTrayIcon->geometry().bottomLeft());
			return;
#endif
			QWidget* w = mPlayer->mainWindow();
			if ( w->isVisible() )
				w->hide();
			else
			{
				w->show();
				w->activateWindow();
				w->raise();
			}
		}
	}

	bool TrayIcon::eventFilter(QObject* obj, QEvent* event )
	{
		if ( obj == mTrayIcon && event->type() == QEvent::Wheel )
		{
			QWheelEvent* wheel = dynamic_cast<QWheelEvent*>(event);
			Q_ASSERT(wheel);

			const qreal step = 0.05;

			if ( wheel->delta() > 0 )
				mPlayer->setVolume( qMin( qreal(1.0), mPlayer->volume() + step ) );
			else
				mPlayer->setVolume( qMax( qreal(0.0), mPlayer->volume() - step ) );

			return true;
		}
		else
		{
			return QObject::eventFilter(obj, event);
		}
	}
}

Q_EXPORT_PLUGIN2(Jerboa_TrayIcon, Jerboa::TrayIcon);
