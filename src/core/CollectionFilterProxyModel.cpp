/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CollectionFilterProxyModel.h"

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QStringList>

#include "CollectionItem.h"
#include "SqlQuery.h"

namespace Jerboa
{
	CollectionFilterProxyModel::CollectionFilterProxyModel(const QString& dbConnectionName, QObject* parent) : QSortFilterProxyModel(parent)
	{
		mDB = QSqlDatabase::cloneDatabase(QSqlDatabase::database(), dbConnectionName);
		mDB.open();
		setFilterString("");

		SqlQuery query(mDB);
		query.exec("SELECT COUNT(ID) + COUNT(DISTINCT Album) + COUNT(DISTINCT AlbumArtist) FROM TrackData");
		query.first();
		mMatchingIDs.reserve(int(query.record().value(0).value<int>() * 1.1));
	};

	void CollectionFilterProxyModel::setFilterString(const QString& string)
	{
		QString whereClause;

		if ( string == "" )
			whereClause = "1";
		else
		{
			QStringList whereClauses;
			QStringList keywords = string.split(" ");
			Q_FOREACH(QString keyword, keywords)
			{
				keyword.replace("'", "\\'");
				keyword = "'%" + keyword + "%'";
				whereClauses.append(QString("SearchKey LIKE %1").arg(keyword));
			}
			whereClause = whereClauses.join(" AND ");
		}
		mMatchingIDs.clear();

		SqlQuery query(mDB);

		query.exec("DROP TABLE IF EXISTS MatchingAlbumArtists");
		query.exec("DROP TABLE IF EXISTS MatchingAlbums");
		query.exec("DROP TABLE IF EXISTS MatchingTracks");

		query.exec(QString("CREATE TEMPORARY TABLE MatchingTracks AS SELECT DISTINCT ID AS ID, Album AS Album \
				FROM Tracks WHERE %1").arg(whereClause));
		query.exec("CREATE TEMPORARY TABLE MatchingAlbums AS SELECT DISTINCT Album AS ID FROM MatchingTracks");
		query.exec("CREATE TEMPORARY TABLE MatchingAlbumArtists AS SELECT DISTINCT Artist AS ID FROM Albums \
				JOIN MatchingAlbums ON Albums.ID = MatchingAlbums.ID");

		query.exec("SELECT ID FROM MatchingAlbumArtists");
		for(query.first(); query.isValid(); query.next())
			mMatchingIDs << CollectionItem::getRef(CollectionItem::ArtistItem, query.record().value(0).value<quint32>());

		query.exec("SELECT ID FROM MatchingAlbums");
		for(query.first(); query.isValid(); query.next())
			mMatchingIDs << CollectionItem::getRef(CollectionItem::AlbumItem, query.record().value(0).value<quint32>());

		query.exec("SELECT ID FROM MatchingTracks");
		for(query.first(); query.isValid(); query.next())
			mMatchingIDs << CollectionItem::getRef(CollectionItem::TrackItem, query.record().value(0).value<quint32>());
		invalidateFilter();
	};

	bool CollectionFilterProxyModel::filterAcceptsRow(int row, const QModelIndex& parent) const
	{
		return mMatchingIDs.contains(sourceModel()->index(row, 0, parent).internalId());
	};
};

