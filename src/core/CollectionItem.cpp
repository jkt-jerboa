/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "CollectionItem.h"

// Ref is a 32-bit var containing:
// 2 bits: ItemType
// 30 bits: unsigned integer track number
//
// This is to fit with one of the QModelIndex::internalId() functions giving a quint32
//
#include <QDebug>

namespace Jerboa
{
	CollectionItem::CollectionItem(){};

	CollectionItem::CollectionItem(quint32 ref)
	{
		mType = (ItemType)(ref >> 30);
		mID = (ref) & (~quint32(0) >> 2);
		mRef = ref;
	};

	CollectionItem::CollectionItem(ItemType type, quint32 id)
	{
		mType = type;
		mID = id;
		mRef = getRef(type, id);
	};
	
	CollectionItem::ItemType CollectionItem::type() const { return mType; };
	quint32 CollectionItem::id() const { return mID; };
	quint32 CollectionItem::ref() const { return mRef; };

	QString CollectionItem::table() const
	{
		switch(mType)
		{
			case ArtistItem:
				return "Artists";
			case AlbumItem:
				return "Albums";
			case TrackItem:
				return "Tracks";
			default:
				return "FAIL!";
		}
	}

	quint32 CollectionItem::getRef(ItemType type, quint32 id)
	{
		return (quint32(type) << 30) | id;
	}
};
