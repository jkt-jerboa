/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "SqlQuery.h"

#include <QDebug>
#include <QSqlError>
#ifdef Q_OS_LINUX
#include <execinfo.h>
#endif

namespace Jerboa
{
	SqlQuery::SqlQuery() : QSqlQuery() {}
	SqlQuery::SqlQuery(QSqlDatabase db) : QSqlQuery(db) {}
	
	bool SqlQuery::exec(const QString& query)
	{
		if ( query.isEmpty() ) return true;
		if ( ! QSqlQuery::exec(query) )
		{
			printError();
			return false;
		}
		return true;
	}

	bool SqlQuery::exec()
	{
		if ( ! QSqlQuery::exec() )
		{
			printError();
			return false;
		}
		return true;
	}

	void SqlQuery::printError()
	{
		qDebug() << "SQL error:";
		qDebug() << "\tQuery:" << lastQuery();
		qDebug() << "\tError:" << lastError();
#ifdef Q_OS_LINUX
		qDebug() << "\tBacktrace:";
		void *buffer[100];
		int size = backtrace(buffer, 100);
		char** strings = backtrace_symbols(buffer, size);
		for(int i = 0; i < size; i++)
			qDebug() << i << strings[i];
#endif
	}
}
