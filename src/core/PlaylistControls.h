/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_PLAYLIST_CONTROLS_H
#define _JERBOA_PLAYLIST_CONTROLS_H

#include "PlaylistInterface.h"
#include "TrackData.h"

namespace Jerboa
{
	class PlaylistModel;
	class PlaylistControls : public PlaylistInterface
	{
		Q_OBJECT;
		public:
			PlaylistControls(PlaylistModel*, QObject* parent = NULL);
		public slots:
			void removeTrack(quint32 position);
			int appendTrack(QUrl url);
			quint32 length();
			int currentTrack();
			const TrackData& trackData(quint32 position);
		private:
			PlaylistModel* mPm;
			TrackData mTd;
	};
}

#endif
