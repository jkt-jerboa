/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_FIRST_RUN_WIZARD
#define _JERBOA_FIRST_RUN_WIZARD

#include <QWizard>

class QLineEdit;
class QWizardPage;

namespace Jerboa
{
	/// Wizard presenting configuration options on the first run.
	class FirstRunWizard : public QWizard
	{
		Q_OBJECT
		public:
			/// Construct the first run wizard.
			FirstRunWizard(QWidget* parent = NULL);
		private slots:
			void browseForCollectionDirectory();
		private:
			/** Get the first page.
			 * The first page contains introduction text, and the
			 * only critical option - the collection location.
			 */
			QWizardPage* createIntroductionPage();

			/// Get the last.fm page.
			QWizardPage* createLastFmPage();

			/** Get the database configuration page.
			 * This is the final page.
			 */
			QWizardPage* createDatabasePage();

			QLineEdit* mCollectionDirectory;
			QLineEdit* mLastFmUser;
			QLineEdit* mLastFmPassword;
	};
};

#endif
