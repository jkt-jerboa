/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Playlist.h"

#include "SqlQuery.h"
#include "TrackDataLoader.h"

#include <QDebug>
#include <QFileInfo>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlField>

typedef QList<Jerboa::TrackData> super;

namespace Jerboa
{
	Playlist::Playlist(const QString& name, QObject* parent)
		:
			QObject(parent),
			QList<TrackData>(),
			m_loaded(false),
			m_name(name),
			m_loader(new TrackDataLoader())
	{
		connect(
			m_loader, SIGNAL(gotTrackData(const QUrl&, const TrackData&)),
			this, SLOT(gotTrackData(const QUrl&, const TrackData&))
		);

		// Get the existing playlist ID, or, if there isn't one, create one.
		SqlQuery query;
		QSqlField field("dbname", QVariant::String);
		field.setValue(name);
		QString safeName = QSqlDatabase::database().driver()->formatValue(field);
		query.exec(
			QString(
				"SELECT `ID` FROM `Playlists` WHERE `Name` = %1"
			).arg(
				safeName
			)
		);
		if(query.first())
		{
			m_playlistId = query.value(0).toInt();
		}
		else
		{
			query.prepare(
				"INSERT INTO `Playlists` "
				"(`Name`, `CurrentTrack`) "
				"VALUES "
				"(:name, -1)"
			);
			query.bindValue(":name", name);
			query.exec();
			m_playlistId = query.lastInsertId().toInt();
		}
		// Get the position
		query.exec(
			QString(
				"SELECT `CurrentTrack` "
				"FROM `Playlists` "
				"WHERE `ID` = %1"
			).arg(m_playlistId)
		);
		query.first();
		m_currentTrack = query.value(0).toInt();

		// Get the files
		query.exec(
			QString(
				"SELECT `FileName` "
				"FROM `PlaylistTracks` "
				"WHERE `Playlist` = %1 "
				"ORDER BY `ID` ASC"
			).arg(
				m_playlistId
			)
		);
		for(query.first(); query.isValid(); query.next())
		{
			m_filesToAdd.append(query.value(0).toString());
		}
		loadNextFile();
	}

	void Playlist::clear()
	{
		super::clear();
		setCurrentTrack(-1);
		updateDatabase();
	}

	void Playlist::gotTrackData(const QUrl& file, const TrackData& data)
	{
		Q_UNUSED(file);
		if(data.isValid())
		{
			super::append(data);
			emit trackAppended(data);
		}
		loadNextFile();
	}

	void Playlist::loadNextFile()
	{
		if(m_filesToAdd.isEmpty())
		{
			return;
		}
		QString fileName = m_filesToAdd.takeFirst();
		QFileInfo info(fileName);
		if(! (info.exists() && info.isFile()))
		{
			loadNextFile();
			return;
		}
		m_loader->readTrackDataFromFile(QUrl::fromLocalFile(fileName));
	}
	
	void Playlist::updateDatabase()
	{
		SqlQuery query;
		query.exec(
			QString(
				"DELETE FROM `PlaylistTracks` "
				"WHERE `Playlist` = %1"
			).arg(
				m_playlistId
			)
		);
		query.prepare(
			"INSERT INTO `PlaylistTracks` "
			"(`Playlist`, `FileName`) "
			"VALUES "
			"(:playlist, :filename)"
		);
		query.bindValue(":playlist", m_playlistId);
		Q_FOREACH(TrackData track, static_cast<super>(*this))
		{
			if(track.url().scheme() == "file")
			{
				query.bindValue(":filename", track.url().toLocalFile());
				query.exec();
			}
		}
	}

	int Playlist::currentTrack() const
	{
		return m_currentTrack;
	}

	void Playlist::setCurrentTrack(int track)
	{
		Q_ASSERT(track >= -1 && track < count());
		m_currentTrack = track;
		SqlQuery query;
		query.prepare(
			QString(
				"UPDATE `Playlists` "
				"SET `CurrentTrack` = :currentTrack "
				"WHERE `ID` = %1"
			).arg(
				m_playlistId
			)
		);
		query.bindValue(":currentTrack", track);
		query.exec();
	}

	void Playlist::removeAt(int i)
	{
		super::removeAt(i);
		updateDatabase();
	}

	void Playlist::insert(int i, TrackData data)
	{
		super::insert(i, data);
		updateDatabase();
	}
}
