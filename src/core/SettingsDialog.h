#ifndef _JERBOA_SETTINGS_H
#define _JERBOA_SETTINGS_H

#include "ui_Settings.h"

#include <QDialog>

namespace Jerboa
{
	class SettingsDialog : public QDialog
	{
		Q_OBJECT
		public:
			SettingsDialog(QWidget* parent = NULL);
		private slots:
			void load();
			void save();

			void browseForCollection();
		private:
			Ui::SettingsDialog m_ui;
	};
};

#endif
