/* LICENSE NOTICE
	This file is part of Jerboa.  
	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_PLAYLIST_MODEL_H
#define _JERBOA_PLAYLIST_MODEL_H

#include "Playlist.h"
#include "TrackData.h"
#include "Types.h"

#include <QAbstractItemModel>
#include <QList>
#include <QMimeData>
#include <QModelIndex>
#include <QPair>
#include <QSqlDatabase>
#include <QString>
#include <QStringList>
#include <QVariant>

namespace Jerboa
{
	class TrackDataLoader;
	class PlaylistModel : public QAbstractItemModel
	{
		Q_OBJECT;
		public:
			PlaylistModel(const QString& dbConnectionName, QObject* parent = 0);
			
			QVariant data(const QModelIndex &index, int role) const;
			Qt::ItemFlags flags(const QModelIndex& index) const;
			QVariant headerData(int section, Qt::Orientation orientation,
					int role = Qt::DisplayRole) const;
			QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
			QModelIndex parent(const QModelIndex& index) const;
			int rowCount(const QModelIndex &parent = QModelIndex()) const;
			int columnCount(const QModelIndex &parent = QModelIndex()) const;
			virtual bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex());

			QStringList mimeTypes() const;
			virtual QMimeData* mimeData(const QModelIndexList& indexes) const;
			bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent);
			Qt::DropActions supportedDropActions() const;

			void setCurrentItem(const QModelIndex&);
			QModelIndex currentItem() const;
			QModelIndex peekNext() const;
			QModelIndex moveNext();
			QModelIndex peekPrevious() const;
			QModelIndex movePrevious();

			TrackData trackData(const QModelIndex& index) const;

			LoopMode loopMode() const;
			void setLoopMode(LoopMode);

			ShuffleMode shuffleMode() const;
			void setShuffleMode(ShuffleMode);
		public slots:
			void clear();
		protected slots:
			void gotTrackData(const QUrl& file, const TrackData& data);
			void trackAppended();
		protected:
			void setNextTrack();
			void loadNextUrl();
			QSqlDatabase mDB;
			Playlist mTracks;
			int mPosition;
			int mNextPosition;
			LoopMode mLoopMode;
			ShuffleMode mShuffleMode;
			TrackDataLoader* mTrackLoader;
			QList<QPair<QUrl, quint32> > mUrlsToAdd;
	};
};
#endif
