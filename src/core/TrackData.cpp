/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "TrackData.h"
#include "TrackData_p.h"

#include <QDebug>
#include <QStringList>

namespace Jerboa
{
	TrackData::TrackData(
		const QUrl& url,
		const QString& album,
		const QString& albumArtist,
		const QString& albumArtistRomanised,
		const QString& artist,
		const QString& artistRomanised,
		const QString& title,
		quint8 trackNumber,
		qreal albumRG,
		qreal trackRG,
		const QString& musicBrainzID)
		: d(new TrackDataPrivate)
	{
		QStringList mimeData;
		mimeData
			   << url.toString()
			   << album
			   << albumArtist
			   << albumArtistRomanised
			   << artist
			   << artistRomanised
			   << title
			   << QString::number(trackNumber)
			   << QString::number(albumRG)
			   << QString::number(trackRG)
			   << musicBrainzID;
		d->mimeData = mimeData.join("\t");
		d->populateMembers();
		d->valid = true;
	};

	bool TrackData::operator==(const TrackData& other) const {
		if ( ! isValid() ) return ! other.isValid();
		return d->mimeData == other.d->mimeData;
	}

	TrackData::TrackData(const TrackData& original)
		: d(new TrackDataPrivate)
	{
		operator=(original);
	}

	TrackData& TrackData::operator=(const TrackData& other)
	{
		if (this != &other)
		{
			if ( other.isValid() )
			{
				d->mimeData = other.d->mimeData;
				d->populateMembers();
				d->valid = true;
			}
			else
				d->valid = false;
		}
		return *this;
	}

	TrackData::TrackData(const QString& mimeData)
		: d(new TrackDataPrivate)
	{
		d->mimeData = mimeData;
		d->populateMembers();
		d->valid = true;
	};

	TrackData::TrackData() : d(new TrackDataPrivate)
	{
		d->valid = false;
	}

	TrackData::~TrackData() { delete d; };

	bool TrackData::isValid() const { return d->valid; };

	const QUrl& TrackData::url() const { Q_ASSERT(d->valid);  return d->url; };

	const QString& TrackData::album() const { Q_ASSERT(d->valid);  return d->album; };
	const QString& TrackData::albumArtist() const { Q_ASSERT(d->valid);  return d->albumArtist; };
	const QString& TrackData::albumArtistRomanised() const { Q_ASSERT(d->valid);  return d->albumArtistRomanised; };
	const QString& TrackData::artist() const { Q_ASSERT(d->valid);  return d->artist; };
	const QString& TrackData::artistRomanised() const { Q_ASSERT(d->valid);  return d->artistRomanised; };
	const QString& TrackData::title() const { Q_ASSERT(d->valid);  return d->title; };
	quint8 TrackData::trackNumber() const { Q_ASSERT(d->valid);  return d->trackNumber; };
	qreal TrackData::albumRG() const { Q_ASSERT(d->valid);  return d->albumRG; };
	qreal TrackData::trackRG() const { Q_ASSERT(d->valid);  return d->trackRG; };
	const QString& TrackData::musicBrainzID() const { Q_ASSERT(d->valid);  return d->musicBrainzID; };

	const QString& TrackData::mimeData() const { Q_ASSERT(d->valid);  return d->mimeData; };

	QVariantMap TrackData::toMpris() const
	{
		if(! isValid() ) return QVariantMap();
		QVariantMap r;
		r["location"] = url().toString();
		r["title"] = title();
		r["artist"] = artist();
		r["album"] = album();
		r["tracknumber"] = QString::number(trackNumber());
		r["mb track id"] = musicBrainzID();
		return r;
	}
};
