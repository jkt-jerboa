/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef PLAYERWINDOW_H
#define PLAYERWINDOW_H

#include "TrackData.h"
#include "Types.h"

#include <QMainWindow>
#include <QModelIndex>
#include <QPointer>
#include <QQueue>
#include <QSqlDatabase>
#include <QTimer>
#include <phonon/phononnamespace.h>
#include <phonon/mediasource.h>

class QAction;
class QCloseEvent;
class QDir;
class QFileSystemModel;
class QHttp;
class QItemSelection;
class QLineEdit;
class QModelIndex;
class QProgressBar;
class QSplitter;
class QTabWidget;
class QToolBar;
class QToolButton;
class QTreeView;

namespace Phonon
{
	class AudioOutput;
	class MediaObject;
	class SeekSlider;
	class VolumeSlider;
};

namespace Jerboa
{
	class CollectionModel;
	class CollectionFilterProxyModel;
	class CollectionScanner;
	class CollectionSettingsWindow;
	class PlayerControls;
	class PlaylistModel;

	class PlayerWindow : public QMainWindow
	{
		friend class PlayerControls;
		Q_OBJECT
		public:
			enum ToolBarIconSize
			{
				SmallIconSize = 16,
				MediumIconSize = 22,
				LargeIconSize = 32,
				HugeIconSize = 48
			};
			Q_ENUMS(ToolBarIconSize);

			PlayerWindow();
		protected:
			virtual void closeEvent(QCloseEvent * event);
		private slots:
			void toolbarButtonStyleChanged(QAction* action);
			void toolbarIconSizeChanged(QAction* action);
			void showToolBarContextMenu(const QPoint&);
			void collectionSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
			void aboutToFinish(qint32 msecToEnd);
			void collectionScanProgress(quint32 done, quint32 total);
			void collectionScanned();
			void searchTyping();
			void updateSearch();
			void searchingDone();
			void clearPlaylist();
			void collectionItemPressed(const QModelIndex& index);
			void collectionItemClicked(const QModelIndex& index);
			void collectionItemDoubleClicked(const QModelIndex& index);
			void createCollection();
			void clearSearch();
			void playerStateChanged(Phonon::State newState, Phonon::State oldState);
			void playlistItemDoubleClicked(const QModelIndex& index);
			void fileSystemViewItemDoubleClicked(const QModelIndex& index);
	
			void previousTriggered();
			void nextTriggered();
			void pauseTriggered();
			void playTriggered();
			void stopTriggered();
	
			void updateControls();
	
			void playlistRowsInserted(const QModelIndex & parent, int start, int end);
			void setRepeatMode(QAction* active);
			void setShuffleMode(QAction* active);
			void popupSenderMenu();
			void volumeChanged(qreal v);
		private:
			void startCollectionScan();
			void loadPlugins();
			void startTrack();
	
			ToolBarIconSize mToolBarIconSize;

			CollectionSettingsWindow* mCollectionSettings;
	
			void loadSettings();
			void loadCollection();
			void loadCollectionData();
			void setupActions();
			void setupPhonon();
			void setupUI();
	
			bool mExpandArtists;
	
			qint64 mScannedTracks;
			qint64 mTotalTracks;
	
			QTimer mAboutToFinishTimer;
			QTimer mSearchTimer;
	
			// Collection stuff
			QSqlDatabase mDB;
			
			// Phonon stuff
			Phonon::AudioOutput* mAudioOutput;
			Phonon::MediaObject* mMediaObject;
			CollectionScanner* mCollectionLoader;
			QModelIndex mFirstErrorIndex;
	
			// UI stuff
			Phonon::SeekSlider *mSeekSlider;
			Phonon::VolumeSlider *mVolumeSlider;
		
			QAction *mPlayAction;
			QAction *mPauseAction;
			QAction *mStopAction;
			QAction *mNextAction;
			QAction *mPreviousAction;
			QAction *mClearPlaylistAction;
	
			QLineEdit *mSearchEdit;

			QAction *mRepeatMenuAction;
			QAction *mRepeatNoneAction;	
			QAction *mRepeatTrackAction;
			QAction *mRepeatPlaylistAction;

			QAction *mShuffleMenuAction;
			QAction* mShuffleOffAction;
			QAction* mShuffleTracksAction;
			QAction* mShuffleAlbumsAction;
	
			QSplitter* mSplitter;
			QMenu* mToolbarOptionsMenu;
	
			PlaylistModel *mPlaylistModel;
			QTreeView *mPlaylistView;
			CollectionModel *mCollectionModel;
			CollectionFilterProxyModel *mCollectionFilterProxyModel;
			QTreeView *mCollectionView;
			QTabWidget *mCollectionTabs;
			QFileSystemModel *mFileSystemModel;
			QTreeView *mFileSystemView;
			PlayerControls* mPlayerControls;

			QToolBar* mToolBar;
			QProgressBar* mProgressBar;
		signals:
			void trackChanged(const TrackData&);
			void playbackStopped();
			void playbackPaused();
			void playbackStarted(const TrackData&, quint64 length);
			void loopModeChanged(LoopMode);
			void shuffleModeChanged(ShuffleMode);
	};
};

#endif
