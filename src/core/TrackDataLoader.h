/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _TRACK_DATA_LOADER_H
#define _TRACK_DATA_LOADER_H

#include "TrackData.h"

#include <QObject>

namespace Jerboa
{
	class TagResolver;
	/// Class to get TrackData objects from various sources.
	class TrackDataLoader : public QObject
	{
		Q_OBJECT
		public:
			/// Construct a TrackDataLoader object
			TrackDataLoader(QObject* parent = NULL);
			/// Get a TrackData object from a TagResolver object.
			static TrackData trackDataFromTagResolver(const TagResolver& resolver);
		public slots:
			/** Start reading TrackData from a file.
			 * @param file is the local file to read from. Remote
			 * 	URLs may be supported in the future.
			 * @see gotTrackData (signal)
			 */
			void readTrackDataFromFile(const QUrl& file);
		signals:
			/** TrackData has been retrieved.
			 * This signal will be emitted when an asynchronous
			 * fetch of TrackData has finished, such as
			 * readTrackDataFromFile.
			 */
			void gotTrackData(const QUrl& file, const TrackData& data);
		private slots:
			void tagsRead();
		private:
			TagResolver* m_tagResolver;
	};
};

#endif
