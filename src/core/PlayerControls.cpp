/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "PlayerControls.h"

#include "PlayerWindow.h"
#include "PlaylistControls.h"

#include <QAction>
#include <phonon/audiooutput.h>
#include <phonon/mediaobject.h>

namespace Jerboa
{
	PlayerControls::PlayerControls(PlayerWindow* parent)
		:
			PlayerInterface(parent),
			w(parent),
			mPlaylistInterface(new PlaylistControls(w->mPlaylistModel, this))
	{}

	QWidget* PlayerControls::mainWindow() const { return w; }

	void PlayerControls::updateActions(Actions available)
	{
		emit availableActionsChanged(available);
	}

	void PlayerControls::play() { emit playTriggered(); }
	void PlayerControls::pause() { emit pauseTriggered(); }
	void PlayerControls::stop() { emit stopTriggered(); }
	void PlayerControls::skipNext() { emit skipNextTriggered(); }
	void PlayerControls::skipPrevious() { emit skipPreviousTriggered(); }

	void PlayerControls::setPosition(quint64 v)	{ w->mMediaObject->seek(qint64(v)); }
	quint64 PlayerControls::position() const { return w->mMediaObject->currentTime(); }

	void PlayerControls::setLoopMode(LoopMode lm)
	{
		switch(lm)
		{
			case LoopNone:
				w->mRepeatNoneAction->trigger();
				break;
			case LoopTrack:
				w->mRepeatTrackAction->trigger();
				break;
			case LoopPlaylist:
				w->mRepeatPlaylistAction->trigger();
				break;
		}
	}
	
	void PlayerControls::setShuffleMode(ShuffleMode sm)
	{
		switch (sm)
		{
			case ShuffleNone:
				w->mShuffleOffAction->trigger();
				break;
			case ShuffleTracks:
				w->mShuffleTracksAction->trigger();
				break;
			case ShuffleAlbums:
				w->mShuffleAlbumsAction->trigger();
				break;
		}
	}

	qreal PlayerControls::volume() const { return w->mAudioOutput->volume(); }
	qreal PlayerControls::volumeDecibel() const { return w->mAudioOutput->volumeDecibel(); }

	void PlayerControls::setVolume(qreal v) { w->mAudioOutput->setVolume(v); }
	void PlayerControls::setVolumeDecibel(qreal v) { w->mAudioOutput->setVolumeDecibel(v); }

	PlaylistInterface* PlayerControls::playlist() const
	{
		return mPlaylistInterface;
	}

	void PlayerControls::updateVolume(qreal f, qreal db)
	{
		emit volumeChanged(f, db);
	}
}
