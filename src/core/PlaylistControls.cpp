/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "PlaylistControls.h"

#include "PlaylistModel.h"

namespace Jerboa
{
	PlaylistControls::PlaylistControls(PlaylistModel* pm, QObject* p)
		:
			PlaylistInterface(p),
			mPm(pm)
	{}

	void PlaylistControls::removeTrack(quint32 position)
	{
		if ( qint32(position) < mPm->rowCount() )
			mPm->removeRows(position, 1);
	}

	int PlaylistControls::appendTrack(QUrl url)
	{
		//FIXME
		return -1;
	}

	quint32 PlaylistControls::length()
	{
		return mPm->rowCount();
	}

	int PlaylistControls::currentTrack()
	{
		if ( mPm->currentItem().isValid() )
			return mPm->currentItem().row();
		else
			return -1;
	}

	const TrackData& PlaylistControls::trackData(quint32 position)
	{
		QModelIndex index = mPm->index(position, 0);
		if ( ! index.isValid() )
			mTd = TrackData();
		else
			mTd = mPm->trackData(index);
		return mTd;
	}
}
