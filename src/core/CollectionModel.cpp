/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "CollectionModel.h"

#include "SqlQuery.h"

#include <QApplication>
#include <QCursor>
#include <QDebug>
#include <QFont>
#include <QHash>
#include <QSqlRecord>
#include <QUrl>

namespace Jerboa
{
	CollectionModel::CollectionModel(QObject* parent) : QAbstractItemModel(parent)
	{
		mAlbumPixmap = QPixmap(":images/media-optical-audio.png");
		mTrackPixmap = QPixmap(":images/audio-basic.png");

		fillCache();
	}

	CollectionItem CollectionModel::item(const QModelIndex& index)
	{
		return CollectionItem(index.internalId());
	}

	QStringList CollectionModel::mimeTypes() const
	{
		QStringList ret;
		ret	<< "text/plain"
			<< "text/uri-list"
			<< "text/x-jerboa-trackdata-list"
			;
		return ret;
	}

	QMimeData* CollectionModel::mimeData(const QModelIndexList& indexes) const
	{
		QMimeData* ret = new QMimeData;
		QStringList plainText;
		QList<QUrl> uris;
		QStringList trackData;

		plainText << "URI\tAlbum\tAlbumArtist\tArtist\tTitle\tTrackNumber";

		SqlQuery query;
		query.prepare("SELECT FileName, Album, AlbumArtist, AlbumArtistRomanised, Artist, ArtistRomanised, "
						"Title, TrackNumber, AlbumRG, TrackRG, MBID FROM TrackData WHERE ID = :id LIMIT 1");
		Q_FOREACH(QModelIndex index, indexes)
		{
			if ( ! index.isValid() ) return ret;
			CollectionItem ci(index.internalId());
			if ( ci.type() != CollectionItem::TrackItem ) continue;
			query.bindValue(":id", ci.id());
			query.exec();
			query.first();
			if ( !query.isValid() ) continue;

			QSqlRecord r = query.record();

			QUrl uri = QUrl::fromLocalFile(r.value(0).toString());
			QString uriString = uri.toEncoded();
			QString album = r.value(1).toString();
			QString albumArtist = r.value(2).toString();
			QString albumArtistRomanised = r.value(3).toString();
			QString artist = r.value(4).toString();
			QString artistRomanised = r.value(5).toString();
			QString title = r.value(6).toString();
			QString trackNumber = r.value(7).toString();
			QString albumRG = r.value(8).toString();
			QString trackRG = r.value(9).toString();
			QString mbid = r.value(10).toString();

			uris << uri;
			QStringList plainTextFields;
			plainTextFields << uriString << album << albumArtist << artist << title << trackNumber;
			plainText << plainTextFields.join("\t");

			QStringList trackDataFields;
			trackDataFields
				<< uriString
				<< album
				<< albumArtist
				<< albumArtistRomanised
				<< artist
				<< artistRomanised
				<< title
				<< trackNumber
				<< albumRG
				<< trackRG
				<< mbid;
			trackData << trackDataFields.join("\t");
		}
		
		ret->setData("text/plain", plainText.join("\n").toUtf8());
		if ( uris.size() == 0 ) return ret;
		ret->setUrls(uris);
		ret->setData("text/x-jerboa-trackdata-list", trackData.join("\n").toUtf8());
		return ret;
	}

	void CollectionModel::addToDataCache(const QString& table, CollectionItem::ItemType type)
	{
		SqlQuery query;
		query.exec(QString("SELECT `ID`, `Name` FROM `%1`").arg(table));
		for (query.first(); query.isValid(); query.next())
		{
			quint32 id = query.record().value(0).toUInt();
			QString name = query.record().value(1).toString();
			quint32 ref = CollectionItem::getRef(type, id);
			mDataCache[ref] = name;
		}	
	}

	void CollectionModel::fillCache()
	{
		QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
		QApplication::processEvents();

		SqlQuery query;
		query.exec("SELECT COUNT(DISTINCT Artist), COUNT(*) FROM Albums");
		query.first();
		quint64 artistCount = query.record().value(0).value<quint64>();
		quint64 albumCount = query.record().value(1).value<quint64>();
		query.exec("SELECT COUNT(*) FROM Tracks");
		query.first();
		quint64 trackCount = query.record().value(0).value<quint64>();
		mDataCache.reserve(artistCount + albumCount + trackCount + 1000);


		addToDataCache("Tracks", CollectionItem::TrackItem);
		addToDataCache("Albums", CollectionItem::AlbumItem);
		addToDataCache("Artists", CollectionItem::ArtistItem);

		// Fill the row cache
		mRowCountCache.clear();
		mRowCountCache.reserve(artistCount + albumCount + 1000);

		mRowCountCache[0] = artistCount;
		// How many albums by each album artist?
		query.exec("SELECT Artist, COUNT(Albums.ID) FROM Albums GROUP BY Artist");
		for(query.first(); query.isValid(); query.next())
			mRowCountCache[CollectionItem::getRef(CollectionItem::ArtistItem, query.record().value(0).toUInt())] = 
				query.record().value(1).toUInt();
		// How many tracks in each album?
		query.exec("SELECT Album, COUNT(*) FROM Tracks GROUP BY Album");
		for(query.first(); query.isValid(); query.next())
			mRowCountCache[CollectionItem::getRef(CollectionItem::AlbumItem, query.record().value(0).toUInt())] = 
				query.record().value(1).toUInt();

		// Fill the index and parent caches
		mIndexCache.clear();
		mParentCache.clear();
		mAlbumTracks.clear();

		mIndexCache.reserve(artistCount + albumCount + trackCount + 1000);
		mParentCache.reserve(albumCount + trackCount + 1000);
		mAlbumTracks.reserve(albumCount + 1000);

		// The obvious way to do this is with three nested loops - however, this has better O(foo).
		QHash<quint32, QModelIndex> refToIndex; // Needed as not doing nested loops.

		quint32 currentParent = ~quint32(0);
		quint32 row = 0;
		QModelIndex index;
		
		// Artists
		query.exec("SELECT DISTINCT Artists.ID FROM Artists JOIN Albums ON Artists.ID = Albums.Artist ORDER BY Artists.SortKey");
		for(query.first(); query.isValid(); query.next())
		{
			quint32 artistRef = CollectionItem::getRef(CollectionItem::ArtistItem, query.record().value(0).toUInt());

			index = createIndex(row, 0, artistRef);
			refToIndex[artistRef] = index;
			mIndexCache[(quint64(~quint32(0)) << 32) | row] = index;

			row++;
		}

		// Albums
		query.exec("SELECT Albums.Artist, Albums.ID, COUNT(Tracks.ID) FROM Albums JOIN Tracks on Albums.ID = Tracks.Album \
				GROUP BY Albums.ID ORDER BY Albums.Artist, Albums.SortKey");
		for(query.first(); query.isValid(); query.next())
		{
			quint32 artistRef = CollectionItem::getRef(CollectionItem::ArtistItem, query.record().value(0).toUInt());
			quint32 albumRef = CollectionItem::getRef(CollectionItem::AlbumItem, query.record().value(1).toUInt());
			mAlbumTracks[albumRef] = query.record().value(2).toUInt();

			if (artistRef != currentParent) row = 0;
			currentParent = artistRef;

			index = createIndex(row, 0, albumRef);
			refToIndex[albumRef] = index;
			mIndexCache[(quint64(artistRef) << 32) | row] = index;
			mParentCache[albumRef] = refToIndex[artistRef];

			row++;
		}

		// Tracks
		query.exec("SELECT Album, ID FROM Tracks ORDER BY Album,TrackNumber");
		for(query.first(); query.isValid(); query.next())
		{
			quint32 albumRef = CollectionItem::getRef(CollectionItem::AlbumItem, query.record().value(0).toUInt());
			quint32 trackRef = CollectionItem::getRef(CollectionItem::TrackItem, query.record().value(1).toUInt());
			if (albumRef != currentParent) row = 0;
			currentParent = albumRef;

			mIndexCache[(quint64(albumRef) << 32) | row] = createIndex(row, 0, trackRef);
			mParentCache[trackRef] = refToIndex[albumRef];

			row++;
		}
		QApplication::restoreOverrideCursor();
		reset();
	}
	
	QVariant CollectionModel::data(const QModelIndex &index, int role) const
	{
		if (!index.isValid())
			return QVariant();

		CollectionItem ci(index.internalId());
		QFont font;
		switch ( role )
		{	
			case Qt::DecorationRole:
				switch ( ci.type() )
				{
					case CollectionItem::AlbumItem:
						return mAlbumPixmap;
					case CollectionItem::TrackItem:
						return mTrackPixmap;
					default:
						return QVariant();
				};
			case Qt::DisplayRole:
				return mDataCache[index.internalId()];
			case Qt::FontRole:
				if ( ci.type() != CollectionItem::ArtistItem) return QVariant();
				font.setBold(true);
				return font;
			default:
				return QVariant();
		};
	}

	Qt::ItemFlags CollectionModel::flags(const QModelIndex &index)
	{
		if ( !index.isValid() ) return Qt::NoItemFlags;
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled;
	}

	QVariant CollectionModel::headerData(int /*section*/, Qt::Orientation /*orientation*/, int /*role*/)
	{
		return QVariant();
	}

	int CollectionModel::columnCount(const QModelIndex&) const { return 1; }

	int CollectionModel::rowCount(const QModelIndex& parent) const
	{
		quint32 ref = parent.isValid() ? parent.internalId() : 0;

		CollectionItem ci(ref);
		if (ci.type() == CollectionItem::TrackItem) return 0;
		return mRowCountCache[ref];
	}

	QModelIndex CollectionModel::index(int row, int column, const QModelIndex& parent) const
	{
		if (! hasIndex(row, column, parent) ) return QModelIndex();

		quint64 ref = row;
		if ( parent.isValid() ) ref |= ((quint64) parent.internalId()) << 32;
		else ref |= (quint64(~quint32(0)) << 32);

		return mIndexCache[ref];
	}

	QModelIndex CollectionModel::parent(const QModelIndex& index) const
	{
		if ( ! index.isValid() ) return QModelIndex();
		CollectionItem ci(index.internalId());

		if ( ci.type() == CollectionItem::ArtistItem ) return QModelIndex();

		quint32 ref = index.internalId();

		return mParentCache[ref];
	}
};

