#include "SettingsDialog.h"

#include <QFileDialog>
#include <QSettings>
#include <QStyle>

namespace Jerboa
{
	SettingsDialog::SettingsDialog(QWidget* parent) : QDialog(parent)
	{
		m_ui.setupUi(this);

		m_ui.browseButton->setIcon(
			style()->standardIcon(QStyle::SP_DirOpenIcon)
		);

		connect(
			m_ui.browseButton,
			SIGNAL(clicked()),
			this,
			SLOT(browseForCollection())
		);
	}

	void SettingsDialog::load()
	{
		QSettings settings;

		// ***** COLLECTION SETTINGS *****

		// Expand artists
		m_ui.expandArtists->setChecked(
			settings.value(
				"collection/expandArtists",
				true
			).toBool()
		);
	}

	void SettingsDialog::browseForCollection()
	{
		QString directory = QDir::toNativeSeparators(
			QFileDialog::getExistingDirectory(
				this,
				tr("Where is your music?"),
				QDir::fromNativeSeparators(m_ui.musicLocation->text())
			)
		);
		if ( directory.isEmpty() ) return;
		m_ui.musicLocation->setText(directory);
	}

	void SettingsDialog::save()
	{
	}
};
