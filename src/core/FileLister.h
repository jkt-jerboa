/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _FILELISTER_H
#define _FILELISTER_H

#include <QDir>
#include <QObject>
#include <QString>
#include <QStringList>

namespace Jerboa
{
	class FileLister : public QObject 
	{
		Q_OBJECT
		public:
			FileLister(QObject* parent);
			void start(const QDir& dir);
			QStringList files();
		public slots:
			void cancel();
		signals:
			void done();
		private:
			bool mCancel;
			void findFiles(const QDir& dir, bool emitDone);
	
			QStringList mFiles;
	};
};

#endif
