/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _TAGRESOLVER_H
#define _TAGRESOLVER_H

#include <QFuture>
#include <QMultiMap>
#include <QObject>
#include <QString>
#include <QStringList>

#include <phonon/mediasource.h>

#include <taglib/tfile.h>
#include <taglib/tstring.h>

namespace Jerboa
{
	class TagResolver : public QObject
	{
		Q_OBJECT
		public:
			TagResolver(QObject* parent);
			void setCurrentSource(Phonon::MediaSource source);
			Phonon::MediaSource currentSource() const;
			QMultiMap<QString, QString> metaData() const;
			QStringList metaData(QString key) const;
			template<class T> T getTag(QString name, T defaultValue) const
			{
					if ( metaData(name).isEmpty() )
							return defaultValue;
					QVariant v = metaData(name).at(0);
					if ( ! v.canConvert<T>() )
							return defaultValue;
					return v.value<T>();
			};
		signals:
			void metaDataChanged();
		private:
			void resolveMetaData();
	
			QMutex workingLock;
			QMultiMap<QString, QString> mMetaData;
			Phonon::MediaSource mMediaSource;
			QFuture<void> mFuture;
	
			TagLib::File* mFile;
	
			TagLib::String getTag(QString tagName, TagLib::String defaultValue, bool &success) const;
			TagLib::String tryApeTag(QString tagName, TagLib::String defaultValue, bool &success) const;
			TagLib::String tryASFTag(QString tagName, TagLib::String defaultValue, bool &success) const;
			TagLib::String tryID3v2Tag(QString tagName, TagLib::String defaultValue, bool &success) const;
			TagLib::String tryXiphTag(QString tagName, TagLib::String defaultValue, bool &success) const;
	};
};

#endif
