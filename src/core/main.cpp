/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>
#include <QIcon>
#include <QStyle>

#include "PlayerWindow.h"
#include "StaticPlugins.h"
#include "Util.h"

int main(int argc, char** argv)
{
	QApplication app(argc,argv);

#ifdef Q_OS_DARWIN
	QStringList libraryPaths = app.libraryPaths();
	libraryPaths.prepend(app.applicationDirPath());
	app.setLibraryPaths(libraryPaths);
#endif

	app.setOrganizationName("Fred Emmott");
	app.setOrganizationDomain("fredemmott.co.uk");
	app.setApplicationName("Jerboa");
	app.setApplicationVersion("git-post-0.2");
	app.setQuitOnLastWindowClosed(true);

	if(!app.style()->inherits("QMacStyle"))
	{
		app.setWindowIcon(QIcon(":images/jerboa.svg"));
	}

	Jerboa::PlayerWindow window;
	window.show();

	return app.exec();
}
