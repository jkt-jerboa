/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_COLLECTION_MODEL_H
#define _JERBOA_COLLECTION_MODEL_H

#include "CollectionItem.h"

#include <QAbstractItemModel>
#include <QHash>
#include <QMimeData>
#include <QModelIndex>
#include <QPixmap>
#include <QString>
#include <QStringList>
#include <QVariant>

namespace Jerboa
{
	class CollectionModel : public QAbstractItemModel
	{
		Q_OBJECT;
		public:
			CollectionModel(QObject* parent = 0);
			
			QMimeData* mimeData(const QModelIndexList& indexes) const;
			QVariant data(const QModelIndex &index, int role) const;
			Qt::ItemFlags flags(const QModelIndex& index);
			QVariant headerData(int section, Qt::Orientation orientation,
					int role = Qt::DisplayRole);
			QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
			QModelIndex parent(const QModelIndex& index) const;
			QStringList mimeTypes() const;
			int rowCount(const QModelIndex &parent = QModelIndex()) const;
			int columnCount(const QModelIndex &parent = QModelIndex()) const;

			CollectionItem item(const QModelIndex& index);
		private:
			QPixmap mAlbumPixmap;
			QPixmap mTrackPixmap;
			void addToDataCache(const QString& table, CollectionItem::ItemType type);
			void fillCache();
			QHash<quint32, QString> mDataCache;
			QHash<quint32, quint32> mAlbumTracks;

			// Cache
			mutable QHash<quint64, QModelIndex> mIndexCache;
			mutable QHash<quint32, int> mRowCountCache;
			mutable QHash<quint32, QModelIndex> mParentCache;
	};
};
#endif
