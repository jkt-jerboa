/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_PLAYER_CONTROLS_H
#define _JERBOA_PLAYER_CONTROLS_H

#include "PlayerInterface.h"

#include <QObject>

namespace Jerboa
{
	class PlaylistInterface;
	class PlayerWindow;
	class PlayerControls : public PlayerInterface
	{
		Q_OBJECT;
		public:
			PlayerControls(PlayerWindow* parent);
			QWidget* mainWindow() const;
			void updateActions(Actions available);
			void updateVolume(qreal fraction, qreal db);
			qreal volume() const;
			qreal volumeDecibel() const;
			quint64 position() const;
			PlaylistInterface* playlist() const;
		public slots:
			void play();
			void pause();
			void stop();
			void skipNext();
			void skipPrevious();
			void setLoopMode(LoopMode);
			void setShuffleMode(ShuffleMode);
			void setVolume(qreal);
			void setVolumeDecibel(qreal);
			void setPosition(quint64);
		signals:
			void playTriggered();
			void pauseTriggered();
			void stopTriggered();
			void skipNextTriggered();
			void skipPreviousTriggered();
		private:
			PlayerWindow* w;
			PlaylistInterface* mPlaylistInterface;
	};
}

#endif
