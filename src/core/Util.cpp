/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Util.h"

#include "config.h"
#include "SqlQuery.h"

#include <QBuffer>
#include <QCoreApplication>
#include <QDateTime>
#include <QDesktopServices>
#include <QDir>
#include <QFile>
#include <QHttp>
#include <QRegExp>
#include <QSize>
#include <QSqlRecord>
#include <QStringList>
#include <QVariant>

using Jerboa::SqlQuery;

namespace Util
{
	QString simpleAlbum(QString album)
	{
		return album.replace(QRegExp("[[(<{].+"), "").trimmed();
	}

	time_t timestamp()
	{
		return QDateTime::currentDateTime().toUTC().toTime_t();
	}

	unsigned int getArtistID(QString artist, QString artistSort)
	{
		SqlQuery query;
		query.prepare("SELECT `ID` FROM `Artists` WHERE `Name` = :artist");
		query.bindValue(":artist", artist);
		query.exec(); query.first();
		if ( query.isValid() )
		{
			// We already have this artist
			return query.record().value(0).toUInt();
		}
		else
		{
			// Need a new artist - firstly, get a romanised form of their name from the sortkey
			QStringList artistRomanisedBackward = artistSort.split(", ");
			QStringList artistRomanisedForward;
			QStringListIterator it(artistRomanisedBackward);
			it.toBack();
			while(it.hasPrevious()) artistRomanisedForward.append(it.previous());
			QString artistRomanised = artistRomanisedForward.join(" ");

			// Now, insert it into the database
			query.prepare("INSERT INTO `Artists` (`Name`, `RomanisedName`, `SortKey`) VALUES (:name, :roman, :sort)");
			query.bindValue(":name", artist);
			query.bindValue(":roman", artistRomanised);
			query.bindValue(":sort", artistSort);
			query.exec();
			return query.lastInsertId().toUInt();
		}
	}

	QString artistName(unsigned int id)
	{
		SqlQuery query;
		query.prepare("SELECT `Name` FROM `Artists` WHERE `ID` = :id");
		query.bindValue(":id", id);
		query.exec(); query.first();
		if ( query.isValid() )
		{
			// We already have this artist
			return query.record().value(0).toString();
		}
		else
		{
			return QString::null;
		}
	}

	QString dataLocation()
	{
#ifdef EMBEDDED_USE_FIXED_PATHS
		return EMBEDDED_FIXED_DATA_PATH;
#else
		return QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#endif
	}

	QString musicLocation()
	{
#ifdef EMBEDDED_USE_FIXED_PATHS
		return EMBEDDED_FIXED_MUSIC_PATH;
#else
		return QDesktopServices::storageLocation(QDesktopServices::MusicLocation);
#endif
	}

	QIcon getIcon(QString name)
	{
		QIcon icon;
		QDir images(":/images/");
		Q_FOREACH(QString item, images.entryList(QDir::AllDirs))
		{
			QFile file(QString(":/images/%1/%2.png").arg(item, name));
			if(file.exists())
			{
				icon.addFile(file.fileName());
			}
			QFile svgz(QString(":/images/%1/%2.svg").arg(item, name));
			if(svgz.exists())
			{
				icon.addFile(svgz.fileName());
			}
		}
		return icon;
	}

	QList<QUrl> walkDirectories(const QFileInfo& fileinfo)
	{
		if (fileinfo.isDir())
		{
			QList<QUrl> list;
			QFileInfoList todo = QDir(fileinfo.absoluteFilePath()).entryInfoList(
					QDir::NoDotAndDotDot | QDir::Dirs | QDir::Files, QDir::Name);
			Q_FOREACH(QFileInfo info, todo)
			{
				if (info.isDir())
					list << walkDirectories(info);
				else
					list << QUrl::fromLocalFile(info.absoluteFilePath());
			}
			return list;
		}
		else
			return QList<QUrl>() << QUrl::fromLocalFile(fileinfo.absoluteFilePath());
	}
};
