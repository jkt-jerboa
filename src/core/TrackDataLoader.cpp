/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "TrackDataLoader.h"
#include "TagResolver.h"

#include <QObject>

#define tr(x) QObject::tr(x)

namespace Jerboa
{
	TrackDataLoader::TrackDataLoader(QObject* parent)
		:
			QObject(parent),
			m_tagResolver(new TagResolver(this))
	{
		connect(
			m_tagResolver,
			SIGNAL(metaDataChanged()),
			this,
			SLOT(tagsRead())
		);
	}

	TrackData TrackDataLoader::trackDataFromTagResolver(const TagResolver& resolver)
	{
		if(resolver.metaData().isEmpty())
		{
			return TrackData();
		}
		QString artist = resolver.getTag<QString>("ARTIST", tr("Unknown"));
		QString artistSort = resolver.getTag<QString>("ARTIST-SORT", artist);
		QString albumName = resolver.getTag<QString>("ALBUM", tr("No Album Title"));
		QString title = resolver.getTag<QString>("TITLE", tr("No Track Title"));
		QString fileName = resolver.currentSource().fileName();
		qint8 trackNumber = resolver.getTag<quint8>("TRACK-NUMBER", 0);
		qreal trackRG = resolver.getTag<qreal>("REPLAYGAIN-TRACK-GAIN", -10000);
		qreal albumRG = resolver.getTag<qreal>("REPLAYGAIN-ALBUM-GAIN", trackRG);
		QString mbid = resolver.getTag<QString>("MUSICBRAINZ-TRACK-ID", "");
		return TrackData(
			QUrl::fromLocalFile(fileName),
			albumName,
			QString(),
			QString(),
			artist,
			artistSort,
			title,
			trackNumber,
			albumRG,
			trackRG,
			mbid
		);
	}

	void TrackDataLoader::readTrackDataFromFile(const QUrl& file)
	{
		if(file.isEmpty() || file.scheme() != "file")
		{
			return;
		}
		m_tagResolver->setCurrentSource(Phonon::MediaSource(file.toLocalFile()));
	}

	void TrackDataLoader::tagsRead()
	{
		emit gotTrackData(QUrl::fromLocalFile(m_tagResolver->currentSource().fileName()), trackDataFromTagResolver(*m_tagResolver));
	}
};
