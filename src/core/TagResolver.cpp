/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "TagResolver.h"

#include <QtConcurrentRun>
#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QFile>

// General taglib
#include <taglib/taglib.h>
#include <taglib/tag.h>
#include <taglib/fileref.h>

// Tag formats
#include <taglib/apetag.h>
#include <taglib/id3v2tag.h>
#include <taglib/xiphcomment.h>

// Hopefully this will get merged into taglib soon, so I can remove this ifdef...
#ifdef HAVE_TAGLIB_ASF
#include <taglib/asftag.h>
#endif

// File formats
#include <taglib/mpegfile.h>
#include <taglib/flacfile.h>

// ID3v2 Stuff
#include <taglib/textidentificationframe.h>

namespace Jerboa
{
	TagResolver::TagResolver(QObject* parent) : QObject(parent) {}
	
	void TagResolver::setCurrentSource(Phonon::MediaSource source)
	{
		mFuture.cancel();
		mMediaSource = source;
		mFuture = QtConcurrent::run(this, &TagResolver::resolveMetaData);
	}

	Phonon::MediaSource TagResolver::currentSource() const
	{
		return mMediaSource;
	}
	
	QMultiMap<QString, QString> TagResolver::metaData() const
	{
		return mMetaData;
	}
	
	QStringList TagResolver::metaData(QString key) const
	{
		return mMetaData.values(key);
	}
	
	TagLib::String TagResolver::tryXiphTag(QString tagName, TagLib::String defaultValue, bool &success) const
	{
		tagName = tagName.replace("-", "_").toLower();
	
		TagLib::Ogg::XiphComment* tag = dynamic_cast<TagLib::Ogg::XiphComment*>(mFile->tag());
		if (!tag)
		{
			TagLib::FLAC::File* flacFile = dynamic_cast<TagLib::FLAC::File*>(mFile);
			if ( flacFile ) tag = flacFile->xiphComment();
		}
		if (!tag) return defaultValue;
	
		TagLib::Ogg::FieldListMap metaData = tag->fieldListMap();
	
		TagLib::Ogg::FieldListMap::ConstIterator it;
		for (it = metaData.begin(); it != metaData.end(); it++ )
			if ( TStringToQString(it->first).toLower() == tagName )
			{
				success = true;
				return it->second[0];
			}
	
		return defaultValue;
	}
	
	TagLib::String TagResolver::tryID3v2Tag(QString tagName, TagLib::String defaultValue, bool &success) const
	{
		tagName = tagName.replace("-", "_").toLower();
	
		TagLib::MPEG::File* file = dynamic_cast<TagLib::MPEG::File*>(mFile);
		if ( !file ) return defaultValue;
	
		TagLib::ID3v2::Tag* tag = file->ID3v2Tag();
		if (!tag) return defaultValue;
	
		TagLib::ID3v2::FrameListMap metaData = tag->frameListMap();
	
		if (!metaData.contains("TXXX")) return defaultValue;
	
		TagLib::ID3v2::FrameList frameList = metaData["TXXX"];
		for (TagLib::ID3v2::FrameList::Iterator it = frameList.begin(); it != frameList.end(); it++)
		{
			TagLib::ID3v2::TextIdentificationFrame* tif = (TagLib::ID3v2::TextIdentificationFrame*) (*it);
			TagLib::StringList fl = tif->fieldList();
			if (TStringToQString(fl.front()).toLower() == tagName)
			{
				success = true;
				return fl.back();
			}
		}
		return defaultValue;
	}
	
	TagLib::String TagResolver::tryApeTag(QString tagName, TagLib::String defaultValue, bool &success) const
	{
		tagName = tagName.replace("-", "_").toLower();
	
		TagLib::MPEG::File* file = dynamic_cast<TagLib::MPEG::File*>(mFile);
		if ( !file ) return defaultValue;
	
		TagLib::APE::Tag* tag = file->APETag();
		if ( !tag ) return defaultValue;
	
		TagLib::APE::ItemListMap map = tag->itemListMap();
		TagLib::APE::ItemListMap::ConstIterator it;
		for ( it = map.begin(); it != map.end(); it++ )
			if ( TStringToQString(it->first).toLower() == tagName )
			{
				success = true;
				return it->second.values()[0];
			}
		return defaultValue;
	}
	
	#ifndef HAVE_TAGLIB_ASF
	TagLib::String TagResolver::tryASFTag(QString /*tagName*/, TagLib::String defaultValue, bool& /*success*/) const
	{
		return defaultValue;
	}
	#else
	TagLib::String TagResolver::tryASFTag(QString tagName, TagLib::String defaultValue, bool &success) const
	{
		tagName = tagName.replace("-", "_").toLower();
	
		TagLib::ASF::Tag* tag = dynamic_cast<TagLib::ASF::Tag*>(mFile->tag());
		if (!tag) return defaultValue;
	
		TagLib::ASF::AttributeListMap metaData = tag->attributeListMap();
	
		TagLib::ASF::AttributeListMap::ConstIterator it;
		for (it = metaData.begin(); it != metaData.end(); it++ )
			if ( TStringToQString(it->first).toLower() == tagName )
			{
				success = true;
				return it->second[0].toString();
			}
	
		return defaultValue;
	}
	#endif
	
	TagLib::String TagResolver::getTag(QString tagName, TagLib::String defaultValue, bool &success) const
	{
		TagLib::String retval;
	
		success = false;
	
		// Look for Xiph Comments
		retval = tryXiphTag(tagName, defaultValue, success);	
	
		// Look for APE
		if (!success)
			retval = tryApeTag(tagName, defaultValue, success);
	
		// Look for ID3v2
		if (!success)
			retval = tryID3v2Tag(tagName, defaultValue, success);
	
		// Look for ASF Tag
		if (!success)
			retval = tryASFTag(tagName, defaultValue, success);

		if (success && !retval.isNull() && !retval.isEmpty()) return retval;
		return defaultValue;
	}
	
	void TagResolver::resolveMetaData()
	{
		mMetaData.clear();
		QString fileName = QDir::toNativeSeparators(mMediaSource.fileName());
		if ( mMediaSource.type() == Phonon::MediaSource::LocalFile )
		{
	#ifdef _WIN32
			TagLib::FileRef f(reinterpret_cast<const wchar_t*>(fileName.utf16()));
	#else
			TagLib::FileRef f(QFile::encodeName(fileName));
	#endif
			TagLib::String tmp;
			uint tmpUI;
			bool success;
	
			if ( f.isNull() || ! f.tag() )
			{
				emit(metaDataChanged());
				return;
			}
			mFile = f.file();
	
			// Artist
			tmp = f.tag()->artist();
			if (!(tmp.isNull() || tmp.isEmpty())) mMetaData.insert("ARTIST", TStringToQString(tmp));
	
			// Album
			tmp = f.tag()->album();
			if (!(tmp.isNull() || tmp.isEmpty())) mMetaData.insert("ALBUM", TStringToQString(tmp));
	
			// Title
			tmp = f.tag()->title();
			if (!(tmp.isNull() || tmp.isEmpty())) mMetaData.insert("TITLE", TStringToQString(tmp));
	
			// Track Number
			tmpUI = f.tag()->track();
			if (tmpUI) mMetaData.insert("TRACK-NUMBER", QString::number(tmpUI));
	
			// ReplayGain Album Gain
			tmp = getTag("REPLAYGAIN-ALBUM-GAIN", TagLib::String::null, success);
			if (success)
				mMetaData.insert("REPLAYGAIN-ALBUM-GAIN", TStringToQString(tmp).replace(" dB", ""));
	
			// ReplayGain Track Gain
			tmp = getTag("REPLAYGAIN-TRACK-GAIN", TagLib::String::null, success);
			if (success)
				mMetaData.insert("REPLAYGAIN-TRACK-GAIN", TStringToQString(tmp).replace(" dB", ""));
	
			// ArtistSort - good for unicode tracks - also good for putting The Beatles in the 'B' section
			tmp = getTag("artistsort", TagLib::String::null, success);
			if (success)
				mMetaData.insert("ARTIST-SORT", TStringToQString(tmp));
			else
			{
				tmp = getTag("WM/ArtistSortOrder", TagLib::String::null, success);
				if (success)
					mMetaData.insert("ARTIST-SORT", TStringToQString(tmp));
			}
	
			tmp = getTag("musicbrainz-trackid", TagLib::String::null, success);
			if (success)
				mMetaData.insert("MUSICBRAINZ-TRACK-ID", TStringToQString(tmp));
		}
		emit(metaDataChanged());
	}
};

