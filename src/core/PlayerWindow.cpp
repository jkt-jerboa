/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PlayerWindow.h"

#include <QAction>
#include <QActionGroup>
#include <QApplication>
#include <QCryptographicHash>
#include <QDateTime>
#include <QDir>
#include <QDrag>
#include <QFileInfo>
#include <QFont>
#include <QHeaderView>
#include <QLabel>
#include <QLineEdit>
#include <QMenu>
#include <QMessageBox>
#include <QMetaEnum>
#include <QMimeData>
#include <QPixmap>
#include <QPluginLoader>
#include <QProgressBar>
#include <QProgressDialog>
#include <QRegExp>
#include <QResource>
#include <QSet>
#include <QSettings>
#include <QSplitter>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlRecord>
#include <QStatusBar>
#include <QString>
#include <QStyle>
#include <QTabWidget>
#include <QToolBar>
#include <QToolButton>
#include <QTreeView>
#include <QVBoxLayout>

#include "config.h"

#include <phonon/audiooutput.h>
#include <phonon/mediaobject.h>
#include <phonon/seekslider.h>
#include <phonon/volumeslider.h>

#include "CollectionFilterProxyModel.h"
#include "CollectionModel.h"
#include "CollectionScanner.h"
#include "CollectionSettingsWindow.h"
#include "FileLister.h"
#include "FirstRunWizard.h"
#include "OneColumnFileSystemModel.h"
#include "PlayerControls.h"
#include "PlaylistModel.h"
#include "PlaylistView.h"
#include "Plugin.h"
#include "SearchLineEdit.h"
#include "SqlQuery.h"
#include "TrackData.h"
#include "TrackDataLoader.h"
#include "Util.h"

namespace Jerboa
{
	PlayerWindow::PlayerWindow()
		:
			QMainWindow(),
			mAudioOutput(NULL),
			mPlayerControls(NULL)
	{
		// Create the data directory
		QDir dir;
		dir.mkpath(Util::dataLocation());
	
		mSearchTimer.setSingleShot(true);
		mSearchTimer.setInterval(FIND_AS_YOU_TYPE_INTERVAL);
		connect(&mSearchTimer, SIGNAL(timeout()), this, SLOT(updateSearch()));
	
		setupActions();
		setupUI();
		loadSettings();
		setupPhonon();
	
		loadCollection();
		mPlaylistModel = new PlaylistModel("PlaylistModel", this);
		connect(mPlaylistModel, SIGNAL(rowsInserted(const QModelIndex&, int, int)), this, SLOT(playlistRowsInserted(const QModelIndex&, int, int)));
		mPlaylistView->setModel(mPlaylistModel);
		loadPlugins();
		updateControls();
		show();

		mFileSystemModel = new OneColumnFileSystemModel(this);
		mFileSystemView->setModel(mFileSystemModel);

		QString rootPath = QSettings().value("collection/directory").toString();
		if (QFileInfo(rootPath).exists())
			mFileSystemView->setRootIndex(mFileSystemModel->setRootPath(rootPath));
		else
			mFileSystemView->setRootIndex(mFileSystemModel->setRootPath(QString::fromAscii("/")));

		mSearchEdit->setFocus(Qt::OtherFocusReason);
	}
	
	void PlayerWindow::playlistRowsInserted(const QModelIndex& parent, int start, int end)
	{
		mFirstErrorIndex = QModelIndex();
		if (
			mMediaObject->state() == Phonon::PausedState
			||
			mMediaObject->state() == Phonon::StoppedState
			||
			mMediaObject->currentSource().type() == Phonon::MediaSource::Invalid
		)
		{
			if(!(mMediaObject->currentSource().type() == Phonon::MediaSource::Invalid && mPlaylistModel->currentItem().row() >= mPlaylistModel->rowCount()))
			{
				mPlaylistModel->setCurrentItem(mPlaylistModel->index(start, 0));
			}
			if(mPlaylistModel->currentItem().isValid() && mPlaylistModel->currentItem().row() < mPlaylistModel->rowCount())
			{
				startTrack();
			}
		}
		updateControls();
	}
	
	void PlayerWindow::closeEvent(QCloseEvent*)
	{
		QSettings settings;
		settings.setValue("geometry/window", saveGeometry());
		settings.setValue("geometry/splitter", mSplitter->saveState());
	}
	
	void PlayerWindow::clearPlaylist()
	{
		mPlaylistModel->clear();
		updateControls();
	}
	
	void PlayerWindow::nextTriggered()
	{
		mAboutToFinishTimer.stop();
		emit playbackStopped();
	
		if (mPlaylistModel->loopMode() == LoopTrack)
			if (qobject_cast<QTimer*>(QObject::sender())) // Was this automatically triggered?
			{
				startTrack();
				return;
			}
	
		if (mPlaylistModel->moveNext().isValid())
		{
			startTrack();
			return;
		}
	
		mMediaObject->setCurrentSource(Phonon::MediaSource());
		if ( mPlaylistModel->currentItem().isValid() )
			mMediaObject->setCurrentSource(mPlaylistModel->trackData(mPlaylistModel->currentItem()).url());
		updateControls();
	}
	
	void PlayerWindow::playlistItemDoubleClicked(const QModelIndex& index)
	{
		Q_ASSERT(mPlaylistModel->trackData(index).isValid());
		emit playbackStopped();
	
		mPlaylistModel->setCurrentItem(index);
		startTrack();
	}
	
	void PlayerWindow::updateControls()
	{
		Actions actions;
		switch( mMediaObject->state() )
		{
			case Phonon::StoppedState:
				if ( mPlaylistModel->currentItem().isValid() )
					actions |= Jerboa::PlayAction;
				break;
			case Phonon::PausedState:
				actions |= Jerboa::PlayAction | Jerboa::StopAction;
				break;
			case Phonon::PlayingState:
				actions |= Jerboa::PauseAction | Jerboa::StopAction;
				break;
			default:
				// nop nop nop
				break;
		}
	
		if (mPlaylistModel->peekPrevious().isValid())
			actions |= SkipPreviousAction;
		if (mPlaylistModel->peekNext().isValid())
			actions |= SkipNextAction;
		mPlayerControls->updateActions(actions);
		mPlayAction->setEnabled(actions & PlayAction);
		mPauseAction->setEnabled(actions & PauseAction);
		mStopAction->setEnabled(actions & StopAction);
		mNextAction->setEnabled(actions & SkipNextAction);
		mPreviousAction->setEnabled(actions & SkipPreviousAction);
	}
	
	void PlayerWindow::collectionScanned()
	{
		CollectionModel* newCollectionModel = new CollectionModel(this);
		mCollectionFilterProxyModel->setSourceModel(newCollectionModel);
		delete mCollectionModel;
		mCollectionModel = newCollectionModel;
		connect(mCollectionView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)), this, SLOT(collectionSelectionChanged(const QItemSelection&, const QItemSelection&)));
		if ( mExpandArtists )
			mCollectionView->expandToDepth(0);
		statusBar()->hide();
	}

	void PlayerWindow::loadCollectionData()
	{
		mCollectionModel = new CollectionModel(this);
		mCollectionFilterProxyModel = new CollectionFilterProxyModel("CollectionFilter", this);
		mCollectionFilterProxyModel->setSourceModel(mCollectionModel);
		mCollectionView->setModel(mCollectionFilterProxyModel);
		connect(mCollectionView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection&, const QItemSelection&)), this, SLOT(collectionSelectionChanged(const QItemSelection&, const QItemSelection&)));
		if ( mExpandArtists )
			mCollectionView->expandToDepth(0);
	}
	
	void PlayerWindow::loadSettings()
	{
		QSettings settings;
		mExpandArtists = settings.value("collection/expandArtists", true).toBool();
		mCollectionView->setRootIsDecorated(! mExpandArtists);
	}
	
	void PlayerWindow::previousTriggered()
	{
		Q_ASSERT(mPlaylistModel->peekPrevious().isValid());
		mAboutToFinishTimer.stop();
		emit playbackStopped();
	
		mPlaylistModel->movePrevious();
		startTrack();
	}
	
	void PlayerWindow::setupActions()
	{
		mPlayAction = new QAction(Util::getIcon("media-playback-start"), tr("Play"), this);
		mPauseAction = new QAction(Util::getIcon("media-playback-pause"), tr("Pause"), this);
		mStopAction = new QAction(Util::getIcon("media-playback-stop"), tr("Stop"), this);
		mNextAction = new QAction(Util::getIcon("media-skip-forward"), tr("Next"), this);
		mPreviousAction = new QAction(Util::getIcon("media-skip-backward"), tr("Previous"), this);
		mClearPlaylistAction = new QAction(Util::getIcon("edit-clear-list"), tr("Clear Playlist"), this);

		mRepeatNoneAction = new QAction(Util::getIcon("media-playlist-repeat-off"), tr("No Repeat"), this);
		mRepeatNoneAction->setCheckable(true);
		mRepeatNoneAction->setChecked(true);
		mRepeatTrackAction = new QAction(Util::getIcon("media-track-repeat"), tr("Repeat Track"), this);
		mRepeatTrackAction->setCheckable(true);
		mRepeatPlaylistAction = new QAction(Util::getIcon("media-playlist-repeat"), tr("Repeat Playlist"), this);
		mRepeatPlaylistAction->setCheckable(true);

		QActionGroup* repeatActions = new QActionGroup(this);
		repeatActions->setExclusive(true);
		repeatActions->addAction(mRepeatNoneAction);
		repeatActions->addAction(mRepeatTrackAction);
		repeatActions->addAction(mRepeatPlaylistAction);

		mRepeatMenuAction = new QAction(this);
		mRepeatMenuAction->setIcon(mRepeatNoneAction->icon());
		mRepeatMenuAction->setText(mRepeatNoneAction->text());
		QMenu* repeatMenu = new QMenu(this);
		repeatMenu->addAction(mRepeatNoneAction);
		repeatMenu->addAction(mRepeatTrackAction);
		repeatMenu->addAction(mRepeatPlaylistAction);
		mRepeatMenuAction->setMenu(repeatMenu);

		mShuffleOffAction = new QAction(Util::getIcon("media-shuffle-off"), tr("No Shuffle"), this);
		mShuffleOffAction->setCheckable(true);
		mShuffleOffAction->setChecked(true);
		mShuffleTracksAction = new QAction(Util::getIcon("media-shuffle-tracks"), tr("Shuffle Tracks"), this);
		mShuffleTracksAction->setCheckable(true);
		mShuffleAlbumsAction = new QAction(Util::getIcon("media-shuffle-albums"), tr("Shuffle Albums"), this);
		mShuffleAlbumsAction->setCheckable(true);

		QActionGroup* shuffleActions = new QActionGroup(this);
		shuffleActions->setExclusive(true);
		shuffleActions->addAction(mShuffleOffAction);
		shuffleActions->addAction(mShuffleTracksAction);
		shuffleActions->addAction(mShuffleAlbumsAction);

		mShuffleMenuAction = new QAction(this);
		mShuffleMenuAction->setIcon(mShuffleOffAction->icon());
		mShuffleMenuAction->setText(mShuffleOffAction->text());
		QMenu* shuffleMenu = new QMenu(this);
		shuffleMenu->addAction(mShuffleOffAction);
		shuffleMenu->addAction(mShuffleTracksAction);
		shuffleMenu->addAction(mShuffleAlbumsAction);
		mShuffleMenuAction->setMenu(shuffleMenu);
	
		connect(mPlayAction, SIGNAL(triggered()), this, SLOT(playTriggered()));
		connect(mPauseAction, SIGNAL(triggered()), this, SLOT(pauseTriggered()));
		connect(mStopAction, SIGNAL(triggered()), this, SLOT(stopTriggered()));
		connect(mNextAction, SIGNAL(triggered()), this, SLOT(nextTriggered()));
		connect(mPreviousAction, SIGNAL(triggered()), this, SLOT(previousTriggered()));
		connect(mClearPlaylistAction, SIGNAL(triggered()), this, SLOT(clearPlaylist()));

		connect(repeatActions, SIGNAL(triggered(QAction*)), this, SLOT(setRepeatMode(QAction*)));
		connect(shuffleActions, SIGNAL(triggered(QAction*)), this, SLOT(setShuffleMode(QAction*)));

		connect(mShuffleMenuAction, SIGNAL(triggered()), this, SLOT(popupSenderMenu()));
		connect(mRepeatMenuAction, SIGNAL(triggered()), this, SLOT(popupSenderMenu()));
	}

	void PlayerWindow::setRepeatMode(QAction* active)
	{
		mRepeatMenuAction->setIcon(active->icon());
		mRepeatMenuAction->setText(active->text());
		LoopMode lm = LoopNone;
		if (active == mRepeatTrackAction)
			lm = LoopTrack;
		else if (active == mRepeatPlaylistAction)
			lm = LoopPlaylist;
		mPlaylistModel->setLoopMode(lm);
		emit loopModeChanged(lm);
		updateControls();
	}

	void PlayerWindow::setShuffleMode(QAction* active)
	{
		mShuffleMenuAction->setIcon(active->icon());
		mShuffleMenuAction->setText(active->text());
		ShuffleMode sm = ShuffleNone;
		if (active == mShuffleTracksAction)
			sm = ShuffleTracks;
		else if (active == mShuffleAlbumsAction)
			sm = ShuffleAlbums;
		mPlaylistModel->setShuffleMode(sm);
		emit shuffleModeChanged(sm);
		updateControls();
	}
	
	void PlayerWindow::clearSearch()
	{
		mSearchEdit->clear();
		QTimer::singleShot(0, this, SLOT(updateSearch()));
	}
	
	void PlayerWindow::playerStateChanged(Phonon::State newState, Phonon::State /*oldState*/)
	{
		TrackData td = mPlaylistModel->trackData(mPlaylistModel->currentItem());
		switch(newState)
		{
			case Phonon::PlayingState:
				mPlaylistView->scrollTo(mPlaylistModel->currentItem(), QAbstractItemView::PositionAtTop);
				if ( td.isValid() )
					emit playbackStarted(td, mMediaObject->totalTime());
				mFirstErrorIndex = QModelIndex();
				break;
			case Phonon::ErrorState:
				if (mFirstErrorIndex == QModelIndex())
				{
					mFirstErrorIndex = mPlaylistModel->currentItem();
					nextTriggered();
					updateControls();
				}
				else if (mFirstErrorIndex == mPlaylistModel->currentItem())
				{
					QMessageBox::warning(this, tr("Playback Failed"), tr("Unable to play any media files"));
				}
				else
				{
					nextTriggered();
					updateControls();
				}
				break;
			default:
				// nop nop nop
				break;
		};
		updateControls();
	}
	
	void PlayerWindow::pauseTriggered()
	{
		mMediaObject->pause();
		emit playbackPaused();
	}
	
	void PlayerWindow::playTriggered()
	{
		mMediaObject->play();
	}
	
	void PlayerWindow::stopTriggered()
	{
		mAboutToFinishTimer.stop();
		emit playbackStopped();
		mMediaObject->stop();
	}
	
	void PlayerWindow::setupPhonon()
	{
		mMediaObject = new Phonon::MediaObject(this);
		mMediaObject->setPrefinishMark(1000);
		connect(mMediaObject, SIGNAL(stateChanged(Phonon::State,Phonon::State)), this, SLOT(playerStateChanged(Phonon::State,Phonon::State)));
		connect(mMediaObject, SIGNAL(prefinishMarkReached(qint32)), this, SLOT(aboutToFinish(qint32)));
		connect(&mAboutToFinishTimer, SIGNAL(timeout()), this, SLOT(nextTriggered()));
		mAboutToFinishTimer.setSingleShot(true);
		mSeekSlider->setMediaObject(mMediaObject);
	
		mAudioOutput = new Phonon::AudioOutput(Phonon::MusicCategory, this);
		connect(mAudioOutput, SIGNAL(volumeChanged(qreal)), this, SLOT(volumeChanged(qreal)));
		Phonon::createPath(mMediaObject, mAudioOutput);
	
		mCollectionLoader = new CollectionScanner(this);
		connect(
			mCollectionLoader, SIGNAL(progressChanged(quint32, quint32)),
			this, SLOT(collectionScanProgress(quint32, quint32))
		);
		connect(
			mCollectionLoader, SIGNAL(finished()),
			this, SLOT(collectionScanned())
		);

		mVolumeSlider->setAudioOutput(mAudioOutput);
		mVolumeSlider->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	}
	
	void PlayerWindow::loadCollection()
	{
		QSettings settings;
	
		mCollectionSettings = new CollectionSettingsWindow(this);
		if(QApplication::arguments().contains("--first-run"))
		{
			//@todo - eventually show this as the real first run wizard.
			QWizard* firstRunWizard = new FirstRunWizard(this);
			firstRunWizard->show();
		}
		if (settings.value("firstRun", true).toBool())
		{
			connect(mCollectionSettings, SIGNAL(rejected()), this, SLOT(close()));
			connect(mCollectionSettings, SIGNAL(accepted()), this, SLOT(createCollection()));
			mCollectionSettings->show();
		}
		else
		{
			if ( ! mDB.isValid() )
			{
				mDB = QSqlDatabase::addDatabase(settings.value("database/type").toString());
				mDB.setHostName(settings.value("database/host").toString());
				mDB.setDatabaseName(settings.value("database/name").toString());
				mDB.setUserName(settings.value("database/user").toString());
				mDB.setPassword(settings.value("database/password").toString());
				if (! mDB.open() ) qFatal(tr("Could not connect to database.").toLocal8Bit());
			}
			startCollectionScan();
		}
	}

	void PlayerWindow::startCollectionScan()
	{
		loadCollectionData();
		mCollectionLoader->run();
		statusBar()->showMessage(tr("Scanning collection..."));
	}
	
	void PlayerWindow::searchTyping()
	{
		mSearchTimer.stop();
		mSearchTimer.start();
	}
	
	void PlayerWindow::updateSearch()
	{
		mCollectionView->setUpdatesEnabled(false);
		mCollectionFilterProxyModel->setFilterString(mSearchEdit->text());
		if ( mSearchEdit->text() != "" || mExpandArtists )
			mCollectionView->expandToDepth(0);
	
		for (int i = 0; i < mCollectionFilterProxyModel->rowCount(); i++) // Artists
		{
			QModelIndex artistItem = mCollectionFilterProxyModel->index(i, 0);
			for (int j = 0; j < mCollectionFilterProxyModel->rowCount(artistItem); j++)
			{
				QModelIndex albumItem = mCollectionFilterProxyModel->index(j, 0, artistItem);
				QModelIndex sourceAlbumItem = mCollectionFilterProxyModel->mapToSource(albumItem);
				if ( mCollectionModel->rowCount(sourceAlbumItem) != mCollectionFilterProxyModel->rowCount(albumItem) )
					mCollectionView->setExpanded(albumItem, true);
			}
		}
		mCollectionView->setUpdatesEnabled(true);	
	}
	
	void PlayerWindow::setupUI()
	{	
		QSettings settings;

		mProgressBar = new QProgressBar(statusBar());
		mProgressBar->setMinimum(0);
		mProgressBar->setMaximum(0);
		statusBar()->addPermanentWidget(mProgressBar);

		mToolBar = new QToolBar(this);
		mToolBar->toggleViewAction()->setVisible(false);
		mToolBar->setContextMenuPolicy(Qt::CustomContextMenu);
		connect(
			mToolBar, SIGNAL(customContextMenuRequested(const QPoint&)),
			this, SLOT(showToolBarContextMenu(const QPoint&))
		);
		addToolBar(mToolBar);
		setUnifiedTitleAndToolBarOnMac(true);

		QString toolButtonStyleString = settings.value(
			"ToolBar/buttonStyle",
			QT_NAME_FROM_ENUM(ToolButtonStyle, Qt::ToolButtonTextUnderIcon)
		).toString();
		setToolButtonStyle(
			QT_ENUM_FROM_NAME(
				ToolButtonStyle,
				toolButtonStyleString
			)
		);
		settings.setValue("ToolBar/buttonStyle", toolButtonStyleString);

		mToolBarIconSize = ENUM_FROM_NAME(
			PlayerWindow,
			ToolBarIconSize,
			settings.value(
				"ToolBar/iconSize",
				NAME_FROM_ENUM(PlayerWindow, ToolBarIconSize, mToolBar->iconSize().width())
			).toString()
		);
		mToolBar->setIconSize(QSize(mToolBarIconSize, mToolBarIconSize));
		settings.setValue("ToolBar/iconSize", NAME_FROM_ENUM(PlayerWindow, ToolBarIconSize, mToolBarIconSize));

		// ToolBar context menu

		mToolbarOptionsMenu = new QMenu(mToolBar);

		// Button style
		QMenu* toolButtonStyleMenu = new QMenu(tr("Toolbar Style"), mToolbarOptionsMenu);
		mToolbarOptionsMenu->addMenu(toolButtonStyleMenu);
		QActionGroup* toolButtonStyleActions = new QActionGroup(toolButtonStyleMenu);

		QAction* toolOptionsAction;
		toolOptionsAction = new QAction(tr("Icons Only"), toolButtonStyleActions);
		toolOptionsAction->setData(static_cast<int>(Qt::ToolButtonIconOnly));
		toolOptionsAction->setCheckable(true);
		toolButtonStyleActions->addAction(toolOptionsAction);
		toolButtonStyleMenu->addAction(toolOptionsAction);

		toolOptionsAction = new QAction(tr("Text Only"), toolButtonStyleActions);
		toolOptionsAction->setData(static_cast<int>(Qt::ToolButtonTextOnly));
		toolOptionsAction->setCheckable(true);
		toolButtonStyleActions->addAction(toolOptionsAction);
		toolButtonStyleMenu->addAction(toolOptionsAction);

		toolOptionsAction = new QAction(tr("Text Beside Icons"), toolButtonStyleActions);
		toolOptionsAction->setData(static_cast<int>(Qt::ToolButtonTextBesideIcon));
		toolOptionsAction->setCheckable(true);
		toolButtonStyleActions->addAction(toolOptionsAction);
		toolButtonStyleMenu->addAction(toolOptionsAction);

		toolOptionsAction = new QAction(tr("Text Under Icons"), toolButtonStyleActions);
		toolOptionsAction->setData(static_cast<int>(Qt::ToolButtonTextUnderIcon));
		toolOptionsAction->setCheckable(true);
		toolButtonStyleActions->addAction(toolOptionsAction);
		toolButtonStyleMenu->addAction(toolOptionsAction);

		// Select the current button style.
		toolButtonStyleActions->actions()[
			QT_ENUM_FROM_NAME(
				ToolButtonStyle,
				toolButtonStyleString
			)
		]->setChecked(true);

		// Hook up the button style menu to the slot that actually changes it.
		connect(
			toolButtonStyleActions,
			SIGNAL(triggered(QAction*)),
			this,
			SLOT(toolbarButtonStyleChanged(QAction*))
		);

		// Icon size
		QMenu* iconSizeMenu = new QMenu(tr("Icon Size"), mToolbarOptionsMenu);
		mToolbarOptionsMenu->addMenu(iconSizeMenu);
		QActionGroup* iconSizeActions = new QActionGroup(iconSizeMenu);

		toolOptionsAction = new QAction(tr("Small Icons"), iconSizeActions);
		toolOptionsAction->setData(static_cast<int>(SmallIconSize));
		toolOptionsAction->setCheckable(true);
		iconSizeActions->addAction(toolOptionsAction);
		iconSizeMenu->addAction(toolOptionsAction);

		toolOptionsAction = new QAction(tr("Medium Icons"), iconSizeActions);
		toolOptionsAction->setData(static_cast<int>(MediumIconSize));
		toolOptionsAction->setCheckable(true);
		iconSizeActions->addAction(toolOptionsAction);
		iconSizeMenu->addAction(toolOptionsAction);

		toolOptionsAction = new QAction(tr("Large Icons"), iconSizeActions);
		toolOptionsAction->setData(static_cast<int>(LargeIconSize));
		toolOptionsAction->setCheckable(true);
		iconSizeActions->addAction(toolOptionsAction);
		iconSizeMenu->addAction(toolOptionsAction);

		toolOptionsAction = new QAction(tr("Huge Icons"), iconSizeActions);
		toolOptionsAction->setData(static_cast<int>(HugeIconSize));
		toolOptionsAction->setCheckable(true);
		iconSizeActions->addAction(toolOptionsAction);
		iconSizeMenu->addAction(toolOptionsAction);

		// Select the current icon size
		Q_FOREACH(QAction* action, iconSizeActions->actions())
		{
			if(action->data().toInt() == mToolBarIconSize)
			{
				action->setChecked(true);
				break;
			}
		}

		// Hook up the icon size menu to the slot that actually changes it.
		connect(
			iconSizeActions,
			SIGNAL(triggered(QAction*)),
			this,
			SLOT(toolbarIconSizeChanged(QAction*))
		);

		// Playback controls
		mToolBar->addAction(mPreviousAction);
		mToolBar->addAction(mPlayAction);
		mToolBar->addAction(mPauseAction);
		mToolBar->addAction(mStopAction);
		mToolBar->addAction(mNextAction);
		// Playlist controls
		QWidget *spacer = new QWidget(this);
		spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
		mToolBar->addWidget(spacer);
		mToolBar->addAction(mShuffleMenuAction);
		mToolBar->addAction(mRepeatMenuAction);
		mToolBar->addAction(mClearPlaylistAction);
		
		// Seek & Volume
		QToolBar *bottomRow = new QToolBar(this);
		mSeekSlider = new Phonon::SeekSlider(this);
		bottomRow->addWidget(mSeekSlider);
		QLabel *volumeLabel = new QLabel(this);
		volumeLabel->setPixmap(QApplication::style()->standardIcon(QStyle::SP_MediaVolume).pixmap(16, 16));
		bottomRow->addWidget(volumeLabel);
		mVolumeSlider = new Phonon::VolumeSlider(this);
		bottomRow->addWidget(mVolumeSlider);

		// Playlist
		mPlaylistView = new PlaylistView(this);
		mPlaylistView->setUniformRowHeights(true);
		mPlaylistView->setDragEnabled(true);
		mPlaylistView->setAcceptDrops(true);
		mPlaylistView->setDropIndicatorShown(true);
		mPlaylistView->setDragDropMode(QAbstractItemView::DragDrop);
		mPlaylistView->setRootIsDecorated(false);
		mPlaylistView->setAllColumnsShowFocus(true);
		mPlaylistView->setSelectionBehavior(QAbstractItemView::SelectRows);
		mPlaylistView->setSelectionMode(QAbstractItemView::ExtendedSelection);
		connect(mPlaylistView, SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(playlistItemDoubleClicked(const QModelIndex&)));

		// Search
		mSearchEdit = new SearchLineEdit(this);
		connect(mSearchEdit, SIGNAL(textEdited(QString)), this, SLOT(searchTyping()));
		connect(mSearchEdit, SIGNAL(returnPressed()), this, SLOT(searchingDone()));

		// Collection
		mCollectionView = new QTreeView(this);
		mCollectionView->setHeaderHidden(true);
		mCollectionView->setUniformRowHeights(true);
		mCollectionView->setDragEnabled(true);
		mCollectionView->setDragDropMode(QAbstractItemView::DragOnly);
		mCollectionView->setSelectionMode(QAbstractItemView::ExtendedSelection);
		mCollectionView->setExpandsOnDoubleClick(false);
		connect(mCollectionView, SIGNAL(pressed(const QModelIndex&)), this, SLOT(collectionItemPressed(const QModelIndex&)));
		connect(mCollectionView, SIGNAL(clicked(const QModelIndex&)), this, SLOT(collectionItemClicked(const QModelIndex&)));
		connect(mCollectionView, SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(collectionItemDoubleClicked(const QModelIndex&)));

		// Filesystem view
		mFileSystemView = new QTreeView(this);
		mFileSystemView->setHeaderHidden(true);
		mFileSystemView->setUniformRowHeights(true);
		mFileSystemView->setDragEnabled(true);
		mFileSystemView->setDragDropMode(QAbstractItemView::DragOnly);
		mFileSystemView->setSelectionMode(QAbstractItemView::ExtendedSelection);
		mFileSystemView->setExpandsOnDoubleClick(false);
		connect(mFileSystemView, SIGNAL(doubleClicked(const QModelIndex&)), this, SLOT(fileSystemViewItemDoubleClicked(const QModelIndex&)));

		// Layout time (no dad, no!)
		QVBoxLayout *playerLayout = new QVBoxLayout();
		playerLayout->addWidget(mPlaylistView);
		playerLayout->addWidget(bottomRow);
		playerLayout->setContentsMargins(4, 4, 4, 4);

		QFrame *playerWidget = new QFrame(this);
		playerWidget->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
		playerWidget->setLayout(playerLayout);

		QVBoxLayout *collectionLayout = new QVBoxLayout();
		collectionLayout->addWidget(mSearchEdit);
		collectionLayout->addWidget(mCollectionView);
		collectionLayout->setContentsMargins(4, 4, 4, 4);

		QWidget *collectionContainer = new QWidget();
		collectionContainer->setLayout(collectionLayout);

		mCollectionTabs = new QTabWidget(this);
		mCollectionTabs->setTabPosition(QTabWidget::West);
		mCollectionTabs->addTab(collectionContainer, tr("Collection"));
		mCollectionTabs->addTab(mFileSystemView, tr("Files"));

		mSplitter = new QSplitter(this);
		mSplitter->addWidget(mCollectionTabs);
		mSplitter->addWidget(playerWidget);

		QVBoxLayout* mainLayout = new QVBoxLayout();
		mainLayout->addWidget(mSplitter);
		QWidget* mainContainer = new QWidget(this);
		mainContainer->setLayout(mainLayout);

		setCentralWidget(mainContainer);
		setWindowTitle(QApplication::applicationName());

		restoreGeometry(settings.value("geometry/window").toByteArray());
		mSplitter->restoreState(settings.value("geometry/splitter").toByteArray());
	}
	
	void PlayerWindow::collectionItemPressed(const QModelIndex& /*index*/)
	{
		QDrag *drag = new QDrag(mCollectionView);
		QItemSelection proxySelection = mCollectionView->selectionModel()->selection();
		QItemSelection sourceSelection = mCollectionFilterProxyModel->mapSelectionToSource(proxySelection);
		drag->setMimeData(mCollectionModel->mimeData(sourceSelection.indexes()));
		drag->exec(Qt::CopyAction);
	}
	
	void PlayerWindow::collectionItemClicked(const QModelIndex& index)
	{
		mCollectionView->selectionModel()->select(index, QItemSelectionModel::Toggle);
	}
	
	void PlayerWindow::collectionItemDoubleClicked(const QModelIndex& index)
	{
		QModelIndexList indexes;
		indexes << index;
		for (int i = 0; i < mCollectionFilterProxyModel->rowCount(index); i++)
		{
			indexes << index.child(i, 0);
			for (int j = 0; j < mCollectionFilterProxyModel->rowCount(index.child(i, 0)); j++)
				indexes << index.child(i, 0).child(j, 0);
		}
		mPlaylistModel->dropMimeData(mCollectionFilterProxyModel->mimeData(indexes), Qt::CopyAction, mPlaylistModel->rowCount(), 0, QModelIndex());
	}

	void PlayerWindow::fileSystemViewItemDoubleClicked( const QModelIndex& index )
	{
		QModelIndexList indexes;
		indexes << index;
		mPlaylistModel->dropMimeData(mFileSystemModel->mimeData(indexes), Qt::CopyAction, mPlaylistModel->rowCount(), 0, QModelIndex());
	}
	
	void PlayerWindow::searchingDone()
	{
		mSearchTimer.stop();
		updateSearch();
		quint32 artistCount = mCollectionFilterProxyModel->rowCount();
		if (artistCount)
		{
			QModelIndexList list;
			for (quint32 i = 0; i < artistCount; i++)
			{
				QModelIndex artist = mCollectionFilterProxyModel->index(i, 0);
				quint32 albumCount = mCollectionFilterProxyModel->rowCount(artist);
				for(quint32 j = 0; j < albumCount; j++)
				{
					QModelIndex album = mCollectionFilterProxyModel->index(j, 0, artist);
					quint32 trackCount = mCollectionFilterProxyModel->rowCount(album);
					for(quint32 k = 0; k < trackCount; k++)
					{
						QModelIndex track = mCollectionFilterProxyModel->index(k, 0, album);
						list << track;
					}
				}
			}
			mPlaylistModel->dropMimeData(
					mCollectionFilterProxyModel->mimeData(list),
					Qt::CopyAction,
					mPlaylistModel->rowCount(),
					0,
					QModelIndex()
			);
		}
		mSearchEdit->clear();
		mCollectionFilterProxyModel->setFilterString("");
		mCollectionView->clearSelection();
		mCollectionView->expandToDepth(0);
	}
	
	void PlayerWindow::createCollection()
	{
		mDB = QSqlDatabase::addDatabase(mCollectionSettings->dbType());
		mDB.setHostName(mCollectionSettings->dbHost());
		mDB.setDatabaseName(mCollectionSettings->dbName());
		mDB.setUserName(mCollectionSettings->dbUser());
		mDB.setPassword(mCollectionSettings->dbPassword());
		if ( ! mDB.open() )
		{
			QMessageBox::warning(this, tr("Invalid database information"), tr("The following error occured while trying to connect to the database: %1").arg(mDB.lastError().text()));
			mCollectionSettings->show();
			return;
		}
	
		QFile dbSetup(":makeDB.sql");
		dbSetup.open(QIODevice::ReadOnly);
		QString instructions = dbSetup.readAll();
		SqlQuery query;
		Q_FOREACH(QString queryString, instructions.split("\n\n"))
			query.exec(queryString);
	
		QSettings settings;
		settings.setValue("database/type", mDB.driverName());
		settings.setValue("database/host", mDB.hostName());
		settings.setValue("database/name", mDB.databaseName());
		settings.setValue("database/user", mDB.userName());
		settings.setValue("database/password", mDB.password());
		settings.setValue("collection/directory", mCollectionSettings->collectionPath());
		settings.setValue("firstRun", false);
		settings.setValue("lastfm/user", mCollectionSettings->lastFMUser());
		settings.setValue("lastfm/password", 
			QCryptographicHash::hash(
			   mCollectionSettings->lastFMPassword().toUtf8(),
			   QCryptographicHash::Md5
			).toHex()
		);

		startCollectionScan();
	}

	void PlayerWindow::collectionScanProgress(quint32 done, quint32 total)
	{
		mProgressBar->setMaximum(total);
		mProgressBar->setValue(done);
	}

	void PlayerWindow::collectionSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
	{
		static QList<quint32> autoSelected;
		static QList<quint32> autoDeSelected;
	
		QItemSelectionModel* sm = mCollectionView->selectionModel();
	
		Q_FOREACH(QModelIndex index, deselected.indexes())
		{
			if (autoDeSelected.contains(index.internalId()))
			{
				autoDeSelected.removeOne(index.internalId());
				continue;
			}
	
			// Deselect all parents
			if (index.parent().isValid())
			{
				sm->select(index.parent(), QItemSelectionModel::Deselect);
				autoDeSelected.append(index.parent().internalId());
				if (index.parent().parent().isValid())
				{
					sm->select(index.parent().parent(), QItemSelectionModel::Deselect);
					autoDeSelected.append(index.parent().parent().internalId());
				}
			}
	
			// Deselect all children
			for (int i = 0; i < mCollectionFilterProxyModel->rowCount(index); i++)
			{
				sm->select(index.child(i, 0), QItemSelectionModel::Deselect);
				autoDeSelected.append(index.child(i,0).internalId());
				for (int j = 0; j < mCollectionFilterProxyModel->rowCount(index.child(i, 0)); j++)
				{
					sm->select(index.child(i, 0).child(j, 0), QItemSelectionModel::Deselect);
					autoDeSelected.append(index.child(i, 0).child(j, 0).internalId());
				}
			}
		}
	
		Q_FOREACH(QModelIndex index, selected.indexes())
		{
			if (autoSelected.contains(index.internalId()))
			{
				autoSelected.removeOne(index.internalId());
				continue;
			}
	
			// Select all children
			for (int i = 0; i < mCollectionFilterProxyModel->rowCount(index); i++)
			{
				sm->select(index.child(i, 0), QItemSelectionModel::Select);
				autoSelected.append(index.child(i,0).internalId());
				for (int j = 0; j < mCollectionFilterProxyModel->rowCount(index.child(i, 0)); j++)
				{
					sm->select(index.child(i, 0).child(j, 0), QItemSelectionModel::Select);
					autoSelected.append(index.child(i, 0).child(j, 0).internalId());
				}
			}
		}
	}
	
	void PlayerWindow::aboutToFinish(qint32 msecToEnd)
	{
		mAboutToFinishTimer.start(msecToEnd);
	}
	
	void PlayerWindow::startTrack()
	{
		Q_ASSERT(mPlaylistModel->currentItem().isValid());
		TrackData td = mPlaylistModel->trackData(mPlaylistModel->currentItem());
	
		mMediaObject->stop();
		if ( td.url().scheme() == "file" )
			mMediaObject->setCurrentSource(Phonon::MediaSource(td.url().toLocalFile()));
		else
			mMediaObject->setCurrentSource(Phonon::MediaSource(td.url()));
		emit trackChanged(td);
		mMediaObject->play();
	
		updateControls();
	}

	void PlayerWindow::popupSenderMenu()
	{
		QAction* action = qobject_cast<QAction*>(sender());
		Q_ASSERT(action);
		QToolButton* button = qobject_cast<QToolButton*>(mToolBar->widgetForAction(action));
		Q_ASSERT(button);
		button->showMenu();
	}

	void PlayerWindow::loadPlugins()
	{
		mPlayerControls = new PlayerControls(this);
		connect(mPlayerControls, SIGNAL(playTriggered()), this, SLOT(playTriggered()));
		connect(mPlayerControls, SIGNAL(pauseTriggered()), this, SLOT(pauseTriggered()));
		connect(mPlayerControls, SIGNAL(stopTriggered()), this, SLOT(stopTriggered()));
		connect(mPlayerControls, SIGNAL(skipNextTriggered()), this, SLOT(nextTriggered()));
		connect(mPlayerControls, SIGNAL(skipPreviousTriggered()), this, SLOT(previousTriggered()));
		connect(this, SIGNAL(trackChanged(const TrackData&)), mPlayerControls, SIGNAL(trackChanged(const TrackData&)));
		connect(this, SIGNAL(playbackStarted(const TrackData&, quint64)), mPlayerControls, SIGNAL(playbackStarted(const TrackData&, quint64)));
		connect(this, SIGNAL(playbackPaused()), mPlayerControls, SIGNAL(playbackPaused()));
		connect(this, SIGNAL(playbackStopped()), mPlayerControls, SIGNAL(playbackStopped()));
		connect(this, SIGNAL(loopModeChanged(LoopMode)), mPlayerControls, SIGNAL(loopModeChanged(LoopMode)));
		connect(this, SIGNAL(shuffleModeChanged(ShuffleMode)), mPlayerControls, SIGNAL(shuffleModeChanged(ShuffleMode)));

		Q_FOREACH(QObject* plugin, QPluginLoader::staticInstances())
		{
			Plugin* p = qobject_cast<Plugin*>(plugin);
			Q_ASSERT(p);
			QWidget* w = p->widget();
			if (w)
			{
				w->setParent(this);
				mCollectionTabs->addTab(w, w->windowTitle());
			}
			p->setPlayerInterface(mPlayerControls);
			qDebug() << "Loaded plugin" << p->pluginName();
		}
	}

	void PlayerWindow::showToolBarContextMenu(const QPoint& pos)
	{
		mToolbarOptionsMenu->popup(mToolBar->mapToGlobal(pos));
	}

	void PlayerWindow::toolbarButtonStyleChanged(QAction* action)
	{
		Qt::ToolButtonStyle value = static_cast<Qt::ToolButtonStyle>(action->data().toInt());
		mToolBar->setToolButtonStyle(value);
		QSettings settings;
		settings.setValue(
			"ToolBar/buttonStyle",
			QT_NAME_FROM_ENUM(ToolButtonStyle, value)
		);
	}

	void PlayerWindow::toolbarIconSizeChanged(QAction* action)
	{
		ToolBarIconSize value = static_cast<ToolBarIconSize>(action->data().toInt());
		mToolBar->setIconSize(QSize(value, value));
		QSettings settings;
		settings.setValue(
			"ToolBar/iconSize",
			NAME_FROM_ENUM(PlayerWindow, ToolBarIconSize, value)
		);
	}

	void PlayerWindow::volumeChanged(qreal v)
	{
		if (mPlayerControls)
			mPlayerControls->updateVolume(v, mAudioOutput->volumeDecibel());
	}
};

