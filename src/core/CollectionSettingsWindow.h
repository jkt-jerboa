/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _COLLECTIONSETTINGSWINDOW_H
#define _COLLECTIONSETTINGSWINDOW_H

#include <QComboBox>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

namespace Jerboa
{
	class CollectionSettingsWindow : public QDialog
	{
		Q_OBJECT
		public:
			CollectionSettingsWindow(QWidget* parent);
			QString collectionPath();
			QString lastFMUser();
			QString lastFMPassword();
			QString dbType();
			QString dbName();
			QString dbHost();
			QString dbUser();
			QString dbPassword();
		private slots:
			void getDirectory();
			void updateOptions();
		private:
			QLineEdit* mCollectionDir;
			QPushButton* mBrowseButton;
			QLineEdit* mLastFMUser;
			QLineEdit* mLastFMPassword;
			QComboBox* mDBType;
			QLineEdit* mDBName;
			QLineEdit* mDBHost;
			QLineEdit* mDBUser;
			QLineEdit* mDBPassword;
	
			QLabel* mDBHostLabel;
			QLabel* mDBUserLabel;
			QLabel* mDBPasswordLabel;
	
			QPushButton* mOKButton;
			QPushButton* mCancelButton;
	};
};

#endif
