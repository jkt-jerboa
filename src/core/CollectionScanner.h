/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _COLLECTION_SCANNER_H
#define _COLLECTION_SCANNER_H

#include <QObject>
#include <QStringList>

class QUrl;

namespace Jerboa
{
	class FileLister;
	class TrackData;
	class TrackDataLoader;
	/// Class for scanning a collection.
	class CollectionScanner : public QObject
	{
		Q_OBJECT
		public:
			/// Construct a CollectionScanner
			CollectionScanner(QObject* parent = NULL);
		public slots:
			/// Start the collection scan
			void run();
		signals:
			/// Emitted when a track is scanned.
			void progressChanged(quint32 done, const quint32 total);
			/// Emitted when scanning is finished.
			void finished();
		private slots:
			/** Called when a file list is retrieved.
			 * This will then start reading the files.
			 */
			void haveFileList();
			/** Called when data for a single file has been read.
			 * This will process the data, and then read the next file.
			 */
			void haveFileData(const QUrl& file, const TrackData& data);
		private:
			FileLister* m_fileLister;
			TrackDataLoader* m_loader;

			quint32 m_progress;
			quint32 m_total;

			QStringList m_filesToRead;
	};
};

#endif
