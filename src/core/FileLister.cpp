/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "FileLister.h"

#include <QDebug>
#include <QtConcurrentRun>
#include <QFileInfo>

namespace Jerboa
{
	FileLister::FileLister(QObject* parent) : QObject(parent) {}
	
	QStringList FileLister::files()
	{
		return mFiles;
	}
	
	void FileLister::start(const QDir& dir)
	{
		mFiles.clear();
		mCancel = false;
		QtConcurrent::run(this, &FileLister::findFiles, dir, true);
	}
	
	void FileLister::cancel()
	{
		mCancel = true;
	}
	
	void FileLister::findFiles(const QDir& dir, bool emitDone)
	{
		QFileInfoList contents = dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot);
		Q_FOREACH(QFileInfo entry, contents)
		{
			if (mCancel) break;
			if ( entry.isDir() )
			{
				QDir subDir = entry.absoluteFilePath();
				findFiles(subDir, false);
			}
			else if ( entry.isReadable() )
				mFiles << entry.absoluteFilePath();
		}
		if (emitDone) emit(done());
	}
};

