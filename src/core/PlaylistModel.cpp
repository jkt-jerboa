/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "PlaylistModel.h"
#include "TrackDataLoader.h"
#include "Util.h"

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QFont>
#include <QPalette>
#include <QSqlError>
#include <QSqlRecord>

namespace Jerboa
{
	PlaylistModel::PlaylistModel(const QString& dbConnectionName, QObject* parent)
		:
			QAbstractItemModel(parent),
			mTrackLoader(new TrackDataLoader(this)),
			mTracks("default", this)
	{
		mDB = QSqlDatabase::cloneDatabase(QSqlDatabase::database(), dbConnectionName);
		mDB.open();
		mPosition = mTracks.currentTrack();
		mNextPosition = -1;

		mLoopMode = LoopNone;
		mShuffleMode = ShuffleNone;
		qsrand((unsigned int) Util::timestamp());
		connect(
			mTrackLoader, SIGNAL(gotTrackData(const QUrl&, const TrackData&)),
			this, SLOT(gotTrackData(const QUrl&, const TrackData&))
		);
		connect(
			&mTracks, SIGNAL(trackAppended(const TrackData&)),
			this, SLOT(trackAppended())
		);
	}

	QVariant PlaylistModel::data(const QModelIndex &index, int role) const
	{
		if ( ! index.isValid() ) return QVariant();

		int row = index.row();
		int column = index.column();

		if ( role == Qt::DisplayRole )
		{
			switch(column)
			{
				case 0:
					return QVariant(mTracks[row].title());
				case 1:
					return QVariant(mTracks[row].artist());
				case 2:
					return QVariant(mTracks[row].album());
				default:
					return QVariant();
			}
		}
		if ( role == Qt::ToolTipRole )
		{
			return QString(
				"<b>%1</b><br/><i>%2 &mdash; %3</i><br/>"
				"<font size='90%'>%4</font>"
			).arg(
				mTracks[row].title()
			).arg(
				mTracks[row].artist()
			).arg(
				mTracks[row].album()
			).arg(
				mTracks[row].url().scheme() == "file"
					? QDir::toNativeSeparators(mTracks[row].url().toLocalFile())
					: mTracks[row].url().toString()
			);
		}
		if (row != mPosition) return QVariant();
		if ( role == Qt::FontRole )
		{
			QFont font;
			font.setBold(true);
			font.setItalic(true);
			return font;
		}
		if ( role == Qt::BackgroundRole )
			return QApplication::palette().alternateBase();
		return QVariant();
	}

	void PlaylistModel::trackAppended()
	{
		beginInsertRows(QModelIndex(), mTracks.size() - 1, mTracks.size() - 1);
		if(mTracks.size() > mPosition)
		{
			setNextTrack();
		}
		endInsertRows();
	}

	Qt::ItemFlags PlaylistModel::flags(const QModelIndex &index) const
	{
		if ( !index.isValid() ) return Qt::ItemIsDropEnabled;
		return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled;
	}

	QStringList PlaylistModel::mimeTypes() const
	{
		QStringList ret;
		ret
			<< "text/x-jerboa-trackdata-list"
			<< "text/x-jerboa-playlist-item-list"
			<< "text/uri-list"
			;
		return ret;
	}
	
	bool PlaylistModel::removeRows(int row, int count, const QModelIndex& parent)
	{
		beginRemoveRows(parent, row, row + (count - 1));
		QSqlDatabase::database().transaction();
		for(int i = 0; i < count; i++)
			mTracks.removeAt(row);
		QSqlDatabase::database().commit();
		endRemoveRows();
		if (mPosition >= row)
		{
			if (mPosition <= row + count - 1)
				setCurrentItem(QModelIndex());
			else
				mPosition -= row;
		}
		setNextTrack();
		return true;
	}

	void PlaylistModel::clear()
	{
		mTracks.clear();
		mNextPosition = -1;
		mPosition = -1;
		reset();
	}
	
	QMimeData* PlaylistModel::mimeData(const QModelIndexList& indexes) const
	{
		QMimeData* ret = new QMimeData();
		QSet<int> IdSet;
		QStringList IDs;
		Q_FOREACH(QModelIndex index, indexes)
		{
			if ( ! IdSet.contains(index.row()) )
				IDs << QString::number(index.row());
			IdSet.insert(index.row());
		};
		ret->setData("text/x-jerboa-playlist-item-list", IDs.join("\n").toUtf8());
		return ret;
	}

	bool PlaylistModel::dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int /*column*/, const QModelIndex& parent)
	{
		if ( data->hasFormat("text/x-jerboa-playlist-item-list") )
		{
			QList<int> rows;
			Q_FOREACH(QString line, QString::fromUtf8(data->data("text/x-jerboa-playlist-item-list")).split("\n"))
				rows.append(line.toInt());
			qSort(rows);
			
			int position = row == -1 ? mTracks.size() : row;
			
			int offset = 0;
			QSqlDatabase::database().transaction();
			Q_FOREACH(int thisRow, rows)
			{
				thisRow += offset;
				
				TrackData td = mTracks[thisRow];
				int oldActive = mPosition;
				removeRow(thisRow);
				if ( position > thisRow ) position--;
				beginInsertRows(QModelIndex(), position, position);
				
				if ( thisRow == oldActive) mPosition = position;
				else if ( thisRow > oldActive && position <= oldActive ) mPosition = ++oldActive;
				else if ( thisRow < oldActive && position >= oldActive ) mPosition = --oldActive;
				mTracks.insert(position, td);

				if (thisRow < position) offset--;
				setNextTrack();
				position++;

				endInsertRows();
			}
			QSqlDatabase::database().commit();
			setCurrentItem(createIndex(mPosition, 0));
			return true;
		}
		else if ( data->hasFormat("text/x-jerboa-trackdata-list") )
		{
			QStringList lines = QString::fromUtf8(data->data("text/x-jerboa-trackdata-list")).split("\n");
			
			int first = row == -1 ? mTracks.size() : row;
			int last = first + lines.size() - 1;
			beginInsertRows(QModelIndex(), first, last);
			
			int position = first;
			QSqlDatabase::database().transaction();
			Q_FOREACH(QString line, lines)
			{
				TrackData td(line);
				mTracks.insert(position++, td);
			}
			QSqlDatabase::database().commit();
			setNextTrack();
			endInsertRows();
			return true;
		}
		if ( data->hasUrls() )
		{
			quint32 fromEnd = row == -1 ? 0 : rowCount() - row;
			Q_FOREACH(QUrl url, data->urls())
			{
				mUrlsToAdd.append(qMakePair(url, fromEnd));
			}
			if(mUrlsToAdd.count() == data->urls().count())
			{
				loadNextUrl();
			}
			return true;
		}
		return false;
	}

	void PlaylistModel::gotTrackData(const QUrl& file, const TrackData& trackData)
	{
		QPair<QUrl, quint32> firstUrl = mUrlsToAdd.takeFirst();
		Q_ASSERT(firstUrl.first == file);
		if(trackData.isValid())
		{
			/// Hack.
			QMimeData data;
			data.setData("text/x-jerboa-trackdata-list", trackData.mimeData().toUtf8());
			dropMimeData(
				&data,
				Qt::CopyAction,
				rowCount() - firstUrl.second,
				0,
				QModelIndex()
			);
		}
		loadNextUrl();
	}

	void PlaylistModel::loadNextUrl()
	{
		if(mUrlsToAdd.isEmpty())
		{
			return;
		}
		QPair<QUrl, quint32> firstUrl = mUrlsToAdd.first();
		QFileInfo info(firstUrl.first.toLocalFile());
		if(info.isFile())
		{
			mTrackLoader->readTrackDataFromFile(firstUrl.first);
		}
		else 
		{
			mUrlsToAdd.takeFirst();
			if(info.isDir())
			{
				QStringList contents(QDir(info.filePath()).entryList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot));
				qSort(contents);
				Q_FOREACH(QString path, contents)
				{
					path = info.filePath() + "/" + path;
					mUrlsToAdd.append(qMakePair(QUrl::fromLocalFile(path), firstUrl.second));
				}
			}
			loadNextUrl();
		}
	}

	QVariant PlaylistModel::headerData(int section, Qt::Orientation orientation, int role) const
	{
		if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
			switch(section)
			{
				case 0:
					return tr("Title");
				case 1:
					return tr("Artist");
				case 2:
					return tr("Album");
				default:
					return QVariant();
			}
		return QVariant();
	}

	int PlaylistModel::columnCount(const QModelIndex&) const { return 3; }

	int PlaylistModel::rowCount(const QModelIndex& parent) const
	{
		if ( parent.isValid() ) return 0;
		return mTracks.size();
	}

	Qt::DropActions PlaylistModel::supportedDropActions() const
	{
		return Qt::CopyAction | Qt::MoveAction;
	}

	QModelIndex PlaylistModel::index(int row, int column, const QModelIndex& parent) const
	{
		if ( parent.isValid() ) return QModelIndex();
		if (! hasIndex(row, column, parent) ) return QModelIndex();

		return createIndex(row, column);
	}

	QModelIndex PlaylistModel::parent(const QModelIndex& /*index*/) const { return QModelIndex(); }
	
	TrackData PlaylistModel::trackData(const QModelIndex& index) const
	{
		if ( ! index.isValid() ) return TrackData();
		Q_ASSERT(index.row() < mTracks.size());
		return mTracks[index.row()];
	}

	QModelIndex PlaylistModel::currentItem() const
	{
		if ( mPosition >= 0) return createIndex(mPosition, 0);
		return QModelIndex();
	}

	void PlaylistModel::setCurrentItem(const QModelIndex& item)
	{
		if ( ! item.isValid() )
		{
			mPosition = -1;
			mTracks.setCurrentTrack(mPosition);
			return;
		}
		Q_ASSERT(item.row() < mTracks.size());

		if (mPosition >= 0 ) emit(dataChanged(index(mPosition, 0), index(mPosition, 2)));
		mPosition = item.row();
		mTracks.setCurrentTrack(mPosition);
		emit(dataChanged(index(mPosition, 0), index(mPosition, 2)));
		setNextTrack();
	}

	void PlaylistModel::setNextTrack()
	{
		if ( mTracks.size() == 0 )
		{
			mNextPosition = -1;
			return;
		}

		switch ( shuffleMode() )
		{
			case ShuffleNone:

				if ( mPosition + 1 < mTracks.size() )
				{
					mNextPosition = mPosition + 1;
					return;
				}

				if ( mLoopMode == LoopPlaylist ) mNextPosition = 0;
				else mNextPosition = -1;
				break;
			case ShuffleTracks:
				mNextPosition = qrand() % mTracks.size();
				/* The following ensures that we don't randomly play
				   the same song twice in a row. */
				if ( mTracks.size() > 1 ) /* A one track playlist will send this loop batty! */
				{
					while ( mNextPosition == mPosition )
					{	
						mNextPosition = qrand() % mTracks.size();
					}
				}
				break;
			case ShuffleAlbums:
				if ( mPosition >= 0 && mPosition + 1 < mTracks.size() &&
					mTracks[mPosition].album() == mTracks[mPosition + 1].album() )
				{
					mNextPosition = mPosition + 1;
					return;
				}
				mNextPosition = qrand() % mTracks.size();
				while (mNextPosition > 0)
				{
					if (mTracks[mNextPosition].album() != mTracks[mNextPosition - 1].album()) return;
					mNextPosition--;
				}
		}
	}

	QModelIndex PlaylistModel::peekNext() const
	{
		if ( mNextPosition == -1 ) return QModelIndex();

		return createIndex(mNextPosition, 0);
	}

	QModelIndex PlaylistModel::moveNext()
	{
		QModelIndex next = peekNext();
		if (! next.isValid() && ! mTracks.isEmpty())
			setCurrentItem(createIndex(0, 0));
		else setCurrentItem(next);
		return next;
	}

	QModelIndex PlaylistModel::peekPrevious() const
	{
		if ( mPosition == -1 ) return QModelIndex();
		if ( mTracks.size() == 0 || (mPosition == 0 && mLoopMode == LoopNone) ) return QModelIndex();

		if ( mPosition == 0 && mLoopMode == LoopPlaylist ) return createIndex(mTracks.size() - 1, 0);
		if ( mPosition != 0 ) return createIndex(mPosition - 1, 0);
		return QModelIndex();
	}

	QModelIndex PlaylistModel::movePrevious()
	{
		QModelIndex previous = peekPrevious();
		setCurrentItem(previous);
		return previous;
	}

	ShuffleMode PlaylistModel::shuffleMode() const
	{
		return mShuffleMode;
	}

	void PlaylistModel::setShuffleMode(ShuffleMode newMode)
	{
		mShuffleMode = newMode;
		setNextTrack();
	}

	LoopMode PlaylistModel::loopMode() const
	{
		return mLoopMode;
	}

	void PlaylistModel::setLoopMode(LoopMode newMode)
	{
		mLoopMode = newMode;
	}
};
