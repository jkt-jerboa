/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_COLLECTIONITEM_H
#define _JERBOA_COLLECTIONITEM_H

#include <QString>

namespace Jerboa
{
	class CollectionItem
	{
		public:
			enum ItemType
			{
				ArtistItem = 1,
				AlbumItem = 2,
				TrackItem = 3
			};

			CollectionItem();
			CollectionItem(quint32 ref);
			CollectionItem(ItemType type, quint32 id);

			ItemType type() const;
			quint32 id() const;
			quint32 ref() const;

			QString table() const;

			static quint32 getRef(ItemType type, quint32 id);
		private:
			ItemType mType;
			quint32 mID;
			qint64 mRef;
	};
};
#endif
