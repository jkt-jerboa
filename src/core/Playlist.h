/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _JERBOA_PLAYLIST_H
#define _JERBOA_PLAYLIST_H

#include "TrackData.h"

#include <QList>

namespace Jerboa
{
	class TrackDataLoader;
	/** Class representing a playlist.
	 * This is a list of tracks, which is synchronised
	 * with the database.
	 *
	 * @todo To avoid surprises, reimplement all insertion/removal members
	 * 	of QList, not just the ones currently used by PlaylistModel.
	 */
	class Playlist : public QObject, public QList<TrackData>
	{
		Q_OBJECT
		public:
			/// Construct a Playlist
			Playlist(const QString& name, QObject* parent = NULL);
			/// Remove an item.
			void removeAt(int position);
			/// Insert an item.
			void insert(int i, TrackData);
			/// Clear the entire list.
			void clear();
			/// The current track in this playlist.
			int currentTrack() const;
			/// Change the current track.
			void setCurrentTrack(int);
		signals:
			void trackAppended(const TrackData&);
		private slots:
			void gotTrackData(const QUrl& file, const TrackData& data);
		private:
			void loadNextFile();
			void updateDatabase();

			bool m_loaded;
			const QString m_name;
			int m_playlistId;
			int m_currentTrack;
			QList<QString> m_filesToAdd;
			TrackDataLoader* m_loader;
	};
};

#endif
