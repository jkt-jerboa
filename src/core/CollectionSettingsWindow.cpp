/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "CollectionSettingsWindow.h"
#include "Util.h"

#include <QFileDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QSettings>
#include <QSpacerItem>
#include <QSqlDatabase>
#include <QVBoxLayout>

namespace Jerboa
{
	CollectionSettingsWindow::CollectionSettingsWindow(QWidget* parent)
		: QDialog(parent)
	{
		QSettings settings;
		setModal(true);
	
		QLabel *pathLabel = new QLabel(tr("Music Location:"));
		mCollectionDir = new QLineEdit(settings.value("collection/directory", Util::musicLocation()).toString());
		mBrowseButton = new QPushButton(QIcon(":/images/document-open-folder.png"), tr("Browse"));
		connect(mBrowseButton, SIGNAL(clicked()), this, SLOT(getDirectory()));
	
		QLabel *dbTypeLabel = new QLabel(tr("Database Type:"));
		mDBType = new QComboBox();
		mDBType->insertItems(0, QSqlDatabase::drivers());
	
		QLabel *dbNameLabel = new QLabel(tr("Database Name:"));
		mDBName = new QLineEdit();
	
		mDBHostLabel = new QLabel(tr("Host:"));
		mDBHost = new QLineEdit("localhost");
	
		mDBUserLabel = new QLabel(tr("Username:"));
		mDBUser = new QLineEdit();
	
		mDBPasswordLabel = new QLabel(tr("Password:"));
		mDBPassword = new QLineEdit();
		mDBPassword->setEchoMode(QLineEdit::Password);
	
		QLabel *lastFMUserLabel = new QLabel(tr("Last.fm User:"));
		mLastFMUser = new QLineEdit();
		
		QLabel *lastFMPasswordLabel = new QLabel(tr("Last.fm Password:"));
		mLastFMPassword = new QLineEdit();
		mLastFMPassword->setEchoMode(QLineEdit::Password);
	
		mOKButton = new QPushButton(tr("OK"));
		mOKButton->setDefault(true);
		mCancelButton = new QPushButton(tr("Cancel"));
	
		// Layout stuff starts here
	
		QGridLayout *optionsLayout = new QGridLayout();
	
		optionsLayout->addWidget(pathLabel, 0, 0);
		optionsLayout->addWidget(mCollectionDir, 0, 1);
		optionsLayout->addWidget(mBrowseButton, 0, 2);
	
		optionsLayout->addWidget(lastFMUserLabel, 1, 0);
		optionsLayout->addWidget(mLastFMUser, 1, 1, 1, 2);
	
		optionsLayout->addWidget(lastFMPasswordLabel, 2, 0);
		optionsLayout->addWidget(mLastFMPassword, 2, 1, 1, 2);
	
		optionsLayout->addWidget(dbTypeLabel, 3, 0);
		optionsLayout->addWidget(mDBType, 3, 1, 1, 2);
	
		optionsLayout->addWidget(dbNameLabel, 4, 0);
		optionsLayout->addWidget(mDBName, 4, 1, 1, 2);
		
		optionsLayout->addWidget(mDBHostLabel, 5, 0);
		optionsLayout->addWidget(mDBHost, 5, 1, 1, 2);
	
		optionsLayout->addWidget(mDBUserLabel, 6, 0);
		optionsLayout->addWidget(mDBUser, 6, 1, 1, 2);
	
		optionsLayout->addWidget(mDBPasswordLabel, 7, 0);
		optionsLayout->addWidget(mDBPassword, 7, 1, 1, 2);
	
		QHBoxLayout *buttonLayout = new QHBoxLayout();
		QSpacerItem *buttonSpacer = new QSpacerItem(0, 0, QSizePolicy::MinimumExpanding);
		buttonLayout->addItem(buttonSpacer);
		buttonLayout->addWidget(mOKButton);
		buttonLayout->addWidget(mCancelButton);
	
		QVBoxLayout *mainLayout = new QVBoxLayout();
		mainLayout->addLayout(optionsLayout);
		QSpacerItem *mainSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
		mainLayout->addItem(mainSpacer);
		mainLayout->addLayout(buttonLayout);
	
		setLayout(mainLayout);
	
		// Slots
		connect(mDBType, SIGNAL(currentIndexChanged(int)), this, SLOT(updateOptions()));
	
		connect(mOKButton, SIGNAL(clicked()), this, SLOT(accept()));
		connect(mCancelButton, SIGNAL(clicked()), this, SLOT(reject()));
	
		// Enable/disable host/username/password settings depending on if we're using SQLite
		updateOptions();
	}
	
	void CollectionSettingsWindow::updateOptions()
	{
		bool sqlite = mDBType->currentText() == "QSQLITE";
		if ( sqlite )
			mDBName->setText(QDir::toNativeSeparators(QString("%1/collection.sqlite").arg(Util::dataLocation())));
		else
			mDBName->setText("Jerboa");
		mDBHost->setDisabled(sqlite);
		mDBUser->setDisabled(sqlite);
		mDBPassword->setDisabled(sqlite);
	
		mDBHostLabel->setDisabled(sqlite);
		mDBUserLabel->setDisabled(sqlite);
		mDBPasswordLabel->setDisabled(sqlite);
	}
	
	void CollectionSettingsWindow::getDirectory()
	{
		QString dir = QDir::toNativeSeparators(QFileDialog::getExistingDirectory(this, tr("Select the directory containing your music collection:"), mCollectionDir->text()));
		if ( dir == "" ) return;
		mCollectionDir->setText(dir);
	}
	
	QString CollectionSettingsWindow::collectionPath() { return QDir::fromNativeSeparators(mCollectionDir->text()); }
	QString CollectionSettingsWindow::dbType() { return mDBType->currentText(); }
	QString CollectionSettingsWindow::dbName() { return mDBName->text(); }
	QString CollectionSettingsWindow::dbHost() { return mDBHost->text(); }
	QString CollectionSettingsWindow::dbUser() { return mDBUser->text(); }
	QString CollectionSettingsWindow::dbPassword() { return mDBPassword->text(); }
	QString CollectionSettingsWindow::lastFMUser() { return mLastFMUser->text(); }
	QString CollectionSettingsWindow::lastFMPassword() { return mLastFMPassword->text(); }
};

