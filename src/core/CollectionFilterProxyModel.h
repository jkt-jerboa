/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option), version 3 of the license.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _JERBOA_COLLECTION_FILTER_PROXY_MODEL_H
#define _JERBOA_COLLECTION_FILTER_PROXY_MODEL_H

#include <QModelIndex>
#include <QSet>
#include <QSortFilterProxyModel>
#include <QSqlDatabase>
#include <QString>

namespace Jerboa
{
	class CollectionFilterProxyModel : public QSortFilterProxyModel
	{
		Q_OBJECT;
		public:
			CollectionFilterProxyModel(const QString& dbConnectionName, QObject* parent = 0);
			void setFilterString(const QString& string);
		protected:
			bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const;
		private:
			QSqlDatabase mDB;
			QSet<int> mMatchingIDs;
	};
};

#endif
