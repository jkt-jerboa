/* LICENSE NOTICE
	This file is part of Jerboa.

	Jerboa is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, version 3.

	Jerboa is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Jerboa.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "FirstRunWizard.h"

#include "Util.h"

#include <QFileDialog>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSettings>
#include <QStyle>
#include <QVBoxLayout>
#include <QWizardPage>

namespace Jerboa
{
	FirstRunWizard::FirstRunWizard(QWidget* parent) : QWizard(parent)
	{
		setWindowTitle("Welcome to Jerboa");
		addPage(createIntroductionPage());
		addPage(createLastFmPage());
		addPage(createDatabasePage());
	}

	QWizardPage* FirstRunWizard::createIntroductionPage()
	{
		QSettings settings;

		QWizardPage* page = new QWizardPage(this);
		page->setTitle(tr("Introduction"));
		page->setFinalPage(true);

		QVBoxLayout* pageLayout = new QVBoxLayout(page);

		QLabel* label;
		label = new QLabel(
			tr(
				"Welcome to Jerboa. This wizard will configure "
				"Jerboa for you.\n\nThe only thing that needs to "
				"be correct is where you keep your music; "
				"please check this below - everything else "
				"is optional, and you can skip it by pressing "
				"the 'Finish' button after checking your music "
				"is in the directory shown below."
			),
			page
		);
		label->setWordWrap(true);
		pageLayout->addWidget(label);

		QHBoxLayout* locationLayout = new QHBoxLayout(page);
		pageLayout->addLayout(locationLayout);

		label = new QLabel(tr("My Music:"), page);
		locationLayout->addWidget(label);
		mCollectionDirectory = new QLineEdit(
			QDir::toNativeSeparators(
				settings.value("collection/directory", Util::musicLocation()).toString()
			),
			page
		);
		locationLayout->addWidget(mCollectionDirectory);
		QPushButton* browseButton = new QPushButton(
			style()->standardIcon(QStyle::SP_DirOpenIcon),
			"",
			page
		);
		browseButton->setToolTip(tr("Browse for folder..."));
		locationLayout->addWidget(browseButton);

		connect(
			browseButton,
			SIGNAL(clicked()),
			this,
			SLOT(browseForCollectionDirectory())
		);

		return page;
	}

	QWizardPage* FirstRunWizard::createLastFmPage()
	{
		QWizardPage* page = new QWizardPage(this);
		page->setTitle(tr("Last.fm"));
		page->setFinalPage(true);

		QVBoxLayout* pageLayout = new QVBoxLayout(page);

		QLabel* label;

		label = new QLabel(
			tr(
				"If you use <a href='http://www.last.fm'>Last.fm</a> "
				"to keep track of what you listen to, enter your "
				"username and password below."
			),
			page
		);
		label->setWordWrap(true);
		label->setOpenExternalLinks(true);
		pageLayout->addWidget(label);

		QGridLayout* lastFmLayout = new QGridLayout(page);
		pageLayout->addLayout(lastFmLayout);
	
		label = new QLabel(tr("Username:"), page);
		lastFmLayout->addWidget(label, 0, 0);

		mLastFmUser = new QLineEdit(page);
		lastFmLayout->addWidget(mLastFmUser, 0, 1);

		label = new QLabel(tr("Password:"), page);
		lastFmLayout->addWidget(label, 1, 0);

		mLastFmPassword = new QLineEdit(page);
		mLastFmPassword->setEchoMode(QLineEdit::Password);
		lastFmLayout->addWidget(mLastFmPassword, 1, 1);

		return page;
	}

	QWizardPage* FirstRunWizard::createDatabasePage()
	{
		QWizardPage* page = new QWizardPage(this);
		page->setTitle(tr("Finished"));
		page->setFinalPage(true);

		QVBoxLayout* pageLayout = new QVBoxLayout(page);

		QLabel* label;

		label = new QLabel(
			tr(
				"Jerboa is now configured; if you would like "
				"to specify which database to use, please use "
				"the advanced options below; otherwise, click "
				"'Finish' to scan your music collection."
			),
			page
		);
		label->setWordWrap(true);
		label->setOpenExternalLinks(true);
		pageLayout->addWidget(label);

		return page;
	}

	void FirstRunWizard::browseForCollectionDirectory()
	{
		QString directory = QDir::toNativeSeparators(
			QFileDialog::getExistingDirectory(
				this,
				tr("Where is your music?"),
				QDir::fromNativeSeparators(mCollectionDirectory->text())
			)
		);
		if ( directory.isEmpty() ) return;
		mCollectionDirectory->setText(directory);
	}
};
