SET(QT_RELEASE_LIBRARIES)
FOREACH(CURRENT_QT_LIBRARY ${QT_LIBRARIES})
	IF(NEXT_QT_LIBRARY_IS_RELEASE)
		SET(QT_RELEASE_LIBRARIES ${QT_RELEASE_LIBRARIES} ${CURRENT_QT_LIBRARY})
		SET(NEXT_QT_LIBRARY_IS_RELEASE FALSE)
	ELSEIF(NEXT_QT_LIBRARY_IS_DEBUG)
		SET(QT_DEBUG_LIBRARIES ${QT_DEBUG_LIBRARIES} ${CURRENT_QT_LIBRARY})
		SET(NEXT_QT_LIBRARY_IS_DEBUG FALSE)
	ENDIF(NEXT_QT_LIBRARY_IS_RELEASE)

	IF(${CURRENT_QT_LIBRARY} STREQUAL "optimized")
		SET(NEXT_QT_LIBRARY_IS_RELEASE TRUE)
	ELSEIF(${CURRENT_QT_LIBRARY} STREQUAL "debug")
		SET(NEXT_QT_LIBRARY_IS_DEBUG TRUE)
	ENDIF(${CURRENT_QT_LIBRARY} STREQUAL "optimized")
ENDFOREACH(CURRENT_QT_LIBRARY ${QT_LIBRARIES})

IF(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
	SET(QT_ACTIVE_LIBRARIES ${QT_DEBUG_LIBRARIES})
ELSE(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
	SET(QT_ACTIVE_LIBRARIES ${QT_RELEASE_LIBRARIES})
ENDIF(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
